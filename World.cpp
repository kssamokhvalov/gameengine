#include "World.h"

World::World()
{
	Vector3 light_direction(1, 1, 1);
	light_direction.ToNormalize();
	infinit_light_sources_.push_back(InfinitLightSource(Color(1.0f, 1.0f, 1.0f), light_direction));
	light_sources_.push_back(&infinit_light_sources_.at(infinit_light_sources_.size() - 1));

	Vector3 position(0.0f, 10.0f, -3.0f);
	point_light_sources_.push_back(PointLightSource(Color(0.5f, 0.0f, 0.0f), position));
	light_sources_.push_back(&point_light_sources_.at(point_light_sources_.size() - 1));

	list_objects_.push_back(Shape::CreateCube(Vector3(1, 1, 3), Quaternion(1, 0, 0, 0), 1));
	list_objects_.push_back(Shape::CreateCube(Vector3(0, -6, 5), Quaternion(1, 0, 0, 0), 10));
	list_objects_.at(1).SetNeedDynamicShadow(false);
}

void World::Update()
{
	for_each(list_objects_.begin(), list_objects_.end(), [](auto& it) {
		it.Action();
	});
}

std::vector<Shape>* World::GetListObject()
{
	return &list_objects_;
}

std::vector<LightSource*>* World::GetListLightSources()
{
	return &light_sources_;
}
