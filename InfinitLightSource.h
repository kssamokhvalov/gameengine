#pragma once
#include "LightSource.h"

class InfinitLightSource :
    public LightSource
{
public:
    InfinitLightSource();
    explicit InfinitLightSource(const Color& light_color, const Vector3& direction_vector);
    Color Shading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const override;
    void CreateShadowVolumes(std::vector<Shape>* list_objects) override;
private:
    Vector3 direction_vector_;
    Color color_of_light_;
    std::vector<std::vector<Edge>> shadows_;
};

