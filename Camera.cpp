#include "Camera.h"
#include<iostream>

Camera::Camera()
{
    viewer_vector_ = Vector3(0, 0, -1);
    position_ = Vector3(0, 0, 0);

    yaw_ = 0;
}


void Camera::IncreaseYaw(const float& delta_yaw)
{
    yaw_ += delta_yaw;
}

void Camera::IncreasePitch(const float& delta_pitch)
{
    pitch_ += delta_pitch;
}

void Camera::MoveFowardOrBackward(const float& delta)
{
    position_.SetZ(position_.GetZ() + delta * cosf(yaw_));
    position_.SetX(position_.GetX() - delta * sinf(yaw_));
}

void Camera::MoveRightOrLeft(const float& delta)
{
    position_.SetZ(position_.GetZ() + delta * sinf(yaw_));
    position_.SetX(position_.GetX() + delta * cosf(yaw_));
}

Matrix4x4 Camera::GetTransformMatrixCameraToWorld() const
{
    Matrix4x4 matrix_transformation_yaw = Matrix4x4::CreateRotationMatrix(Vector3(0, 1, 0), yaw_);
    Matrix4x4 matrix_transformation_pitch = Matrix4x4::CreateRotationMatrix(matrix_transformation_yaw.ProductOnVector3(Vector3(1, 0, 0), 1), pitch_);
    matrix_transformation_pitch.ChangeHandedness();
    return Matrix4x4::CreateTransformationMatrix(matrix_transformation_yaw * matrix_transformation_pitch, position_);
}
