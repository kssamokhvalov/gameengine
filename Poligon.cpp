#include "Poligon.h"

Poligon::Poligon(const int& first_vertex_index, const int& second_vertex_index, const int& third_vertex_index, const PropertyOfPoligon& visual_property)
{
    vertex_index_[0] = first_vertex_index;
    vertex_index_[1] = second_vertex_index;
    vertex_index_[2] = third_vertex_index;
    property_poligon_ = visual_property;
}

int Poligon::GetVertexIndex(const int& num_vertex) const
{
    int index = num_vertex < 0 ? num_vertex + 3 : num_vertex % 3;
    return vertex_index_[index];
}

int Poligon::GetFirstVertexIndex() const
{
    return vertex_index_[0];
}

int Poligon::GetSecondVertexIndex() const
{
    return vertex_index_[1];
}

int Poligon::GetThirdVertexIndex() const
{
    return vertex_index_[2];
}

void Poligon::SetVisualProperty(const PropertyOfPoligon& visual_property)
{
    property_poligon_ = visual_property;
}

const PropertyOfPoligon& Poligon::GetVisualProperty() const
{
    return property_poligon_;
}

void Poligon::SetNormal(const Vector3& normal)
{
    normal_ = normal;
}

const Vector3& Poligon::GetNormal() const
{
    return normal_;
}


