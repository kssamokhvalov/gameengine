#include "Shader.h"

Shader::Shader()
{
}

Shader::Shader(std::vector<LightSource*>* light_sources, const Color& embied_color)
{
	light_sources_ = light_sources;
	embied_color_ = embied_color;

    screen_buffeer_ = std::vector<std::vector<Pixel>>(WIDTH_SCREEN, std::vector<Pixel>(HIGHT_SCREEN, Pixel()));

    frustrum_matrix = Matrix4x4::CreateFrustumProjectionMatrix(3.14 / 2, ASPECT, 0, 30);
    near_plate_ = 1 / tanf(3.14 / 2 / 2);
    inversed_frustrum_matrix = frustrum_matrix.GetInversedMatrix();
    viewer_vector_ = Vector3(0, 0, -1);

    N_near_plate_ = Vector3(0, 0, -1);
    bottom_plate_ = Vector4(0, near_plate_ / sqrtf(near_plate_ * near_plate_ + ASPECT * ASPECT), -ASPECT / sqrtf(near_plate_ * near_plate_ + ASPECT * ASPECT), 0);

    accses_draw = Vector4::Dot(bottom_plate_, Vector4(viewer_vector_, 0));
}

Color Shader::PixelShading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q)  const
{
	return property_poligon.diffuse_reflection_color * embied_color_ + LightShading(property_poligon, normal, Q);
}

void Shader::CreateShadowVoluem(std::vector<Shape>* list_objects)
{
	for_each(light_sources_->begin(), light_sources_->end(), [&](auto& it) {
		it->CreateShadowVolumes(list_objects);
	});
}

void Shader::SetAmbiedColor(const Color& embied_color)
{
	embied_color_ = embied_color;
}

void Shader::SetListLightSources(std::vector<LightSource*>* light_sources)
{
	light_sources_ = light_sources;
}

void Shader::DrawObjects(std::vector<Shape>* list_objects, const Matrix4x4& transform_matrix_camera_to_world)
{
    transform_matrix_camera_to_world_ = transform_matrix_camera_to_world;
    transform_matrix_world_to_camera_ = transform_matrix_camera_to_world_.GetInversedMatrix();

    std::vector<Shape> list_objects_world;

    std::for_each(list_objects->begin(), list_objects->end(), [&](auto& it) {
        list_objects_world.push_back(it.GetWorldObject());
    });

    CreateShadowVoluem(&list_objects_world);

    for_each(list_objects->begin(), list_objects->end(), [this](auto& it) {
        this->RenderObject(it, it.GetTransformationMatrixToWorld());
    });
}

void Shader::RenderObject(const Shape& shape, const Matrix4x4& rotation_matrix)
{
    //Hard Code
    Matrix4x4 view_matrix = rotation_matrix * transform_matrix_world_to_camera_;

    for (int i = 0; i < shape.GetNumPoligons(); i++) {

        if (Vector3::Dot(view_matrix.ProductOnVector3ForVectors(shape.GetPoligon(i).GetNormal()), viewer_vector_) > accses_draw) {
            continue;
        }

        std::vector<Vector3> vertexes;


        vertexes.push_back(view_matrix.ProductOnVector3(shape.GetVertex(shape.GetPoligon(i).GetFirstVertexIndex()).GetPosition(), 1));
        vertexes.push_back(view_matrix.ProductOnVector3(shape.GetVertex(shape.GetPoligon(i).GetSecondVertexIndex()).GetPosition(), 1));
        vertexes.push_back(view_matrix.ProductOnVector3(shape.GetVertex(shape.GetPoligon(i).GetThirdVertexIndex()).GetPosition(), 1));

        std::vector<bool> pass_clipping;
        int num_passed = 0;

        std::for_each(vertexes.begin(), vertexes.end(), [this, &pass_clipping, &num_passed](auto& it) {
            pass_clipping.push_back(this->ClipingTestNearPlate(it));
            num_passed += static_cast<int>(pass_clipping.at(pass_clipping.size() - 1));
        });

        int num_dont_passed = (pass_clipping.size() - num_passed);
        if (num_dont_passed == pass_clipping.size()) {
            continue;
        }
        if (num_dont_passed != 0) {
            for (int k = 0; k < pass_clipping.size(); k++) {
                int prev_vertex = k - 1 >= 0 ? k - 1 : pass_clipping.size() - 1;
                if (!pass_clipping.at(k) && pass_clipping.at(prev_vertex)) {
                    int next_vertex = k + num_dont_passed < pass_clipping.size() ? k + num_dont_passed : k + num_dont_passed - pass_clipping.size();
                    int next_vertex_minus_1 = k + num_dont_passed - 1 < pass_clipping.size() ? k + num_dont_passed - 1 : k + num_dont_passed - pass_clipping.size() - 1;


                    float t = Vector4::Dot(Vector4(N_near_plate_, -near_plate_), Vector4(vertexes.at(next_vertex_minus_1), 1)) / Vector4::Dot(Vector4(N_near_plate_, -near_plate_), Vector4(vertexes.at(next_vertex_minus_1) - vertexes.at(next_vertex), 0));
                    Vector3 W1 = vertexes.at(next_vertex_minus_1) + (vertexes.at(next_vertex) - vertexes.at(next_vertex_minus_1)) * t;
                    t = Vector4::Dot(Vector4(N_near_plate_, -near_plate_), Vector4(vertexes.at(k), 1)) / Vector4::Dot(Vector4(N_near_plate_, -near_plate_), Vector4(vertexes.at(k) - vertexes.at(prev_vertex), 0));
                    Vector3 W2 = vertexes.at(k) + (vertexes.at(prev_vertex) - vertexes.at(k)) * t;

                    vertexes.insert(vertexes.begin() + k, W1);
                    pass_clipping.insert(pass_clipping.begin() + k, true);
                    vertexes.insert(vertexes.begin() + k, W2);
                    pass_clipping.insert(pass_clipping.begin() + k, true);

                    for (int j = pass_clipping.size() - 1; j > -1; j--) {
                        if (!pass_clipping.at(j)) {
                            vertexes.erase(vertexes.begin() + j);
                        }
                    }
                    break;
                }
            }
        }


        for (int k = 1; k < vertexes.size() - 1; k++) {
            Rastirase(frustrum_matrix.ProductOnVector3(vertexes.at(0), 1),
                frustrum_matrix.ProductOnVector3(vertexes.at(k), 1),
                frustrum_matrix.ProductOnVector3(vertexes.at(k + 1), 1),
                view_matrix.ProductOnVector3ForVectors(shape.GetPoligon(i).GetNormal()), shape.GetPoligon(i).GetVisualProperty());
        }
    }
}

std::vector<std::vector<Pixel>>* Shader::GetScreenBuffer()
{
    return &screen_buffeer_;
}

void Shader::Rastirase(const Vector3& first_vertex, const Vector3& second_vertex, const Vector3& third_vertex, const Vector3& normal, const PropertyOfPoligon& property_poligon)
{
	 //Hard Code
    Vector3 N0 = Vector3::Cross((second_vertex - first_vertex), (third_vertex - first_vertex));

    float max_x = FindMax(first_vertex.GetX(), second_vertex.GetX(), third_vertex.GetX());
    float max_y = FindMax(first_vertex.GetY(), second_vertex.GetY(), third_vertex.GetY());

    float min_x = FindMin(first_vertex.GetX(), second_vertex.GetX(), third_vertex.GetX());
    float min_y = FindMin(first_vertex.GetY(), second_vertex.GetY(), third_vertex.GetY());

    max_x = max_x < 1 ? max_x : 1;
    min_x = min_x > -1 ? min_x : -1;

    max_y = max_y < ASPECT ? max_y : 1;
    min_y = min_y > -ASPECT ? min_y : -1;

    int max_coordinate_w = static_cast<int>(max_x * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
    int min_coordinate_w = static_cast<int>(min_x * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
    int max_coordinate_h = static_cast<int>(max_y * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);
    int min_coordinate_h = static_cast<int>(min_y * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);

#ifdef DEBUG_3D
    {
        int pixel_x_seecond_vertex = static_cast<int>(second_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
        int pixel_y_seecond_vertex = static_cast<int>(second_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);
        int pixel_x_first_vertex = static_cast<int>(first_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
        int pixel_y_first_vertex = static_cast<int>(first_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);
        int pixel_x_third_vertex = static_cast<int>(third_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
        int pixel_y_third_vertex = static_cast<int>(third_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);

        if (pixel_x_seecond_vertex < WIDTH_SCREEN && pixel_x_seecond_vertex > 0 && pixel_y_seecond_vertex < HIGHT_SCREEN && pixel_y_seecond_vertex > 0) {
            screen_buffeer_.at(pixel_x_seecond_vertex).at(pixel_y_seecond_vertex).r = 1;
            screen_buffeer_.at(pixel_x_seecond_vertex).at(pixel_y_seecond_vertex).g = 1;
            screen_buffeer_.at(pixel_x_seecond_vertex).at(pixel_y_seecond_vertex).b = 1;
            screen_buffeer_.at(pixel_x_seecond_vertex).at(pixel_y_seecond_vertex).z_buffer = -1;
        }
        if (pixel_x_first_vertex < WIDTH_SCREEN && pixel_x_first_vertex > 0 && pixel_y_first_vertex < HIGHT_SCREEN && pixel_y_first_vertex > 0) {
            screen_buffeer_.at(pixel_x_first_vertex).at(pixel_y_first_vertex).r = 1;
            screen_buffeer_.at(pixel_x_first_vertex).at(pixel_y_first_vertex).g = 1;
            screen_buffeer_.at(pixel_x_first_vertex).at(pixel_y_first_vertex).b = 1;
            screen_buffeer_.at(pixel_x_first_vertex).at(pixel_y_first_vertex).z_buffer = -1;
        }
        if (pixel_x_third_vertex < WIDTH_SCREEN && pixel_x_third_vertex > 0 && pixel_y_third_vertex < HIGHT_SCREEN && pixel_y_third_vertex > 0) {
            screen_buffeer_.at(pixel_x_third_vertex).at(pixel_y_third_vertex).r = 1;
            screen_buffeer_.at(pixel_x_third_vertex).at(pixel_y_third_vertex).g = 1;
            screen_buffeer_.at(pixel_x_third_vertex).at(pixel_y_third_vertex).b = 1;
            screen_buffeer_.at(pixel_x_third_vertex).at(pixel_y_third_vertex).z_buffer = -1;
        }

/*

        for (int k = min_coordinate_w; k < max_coordinate_w; k++) {
            screen_buffeer_[k][max_coordinate_h].r = 1;
            screen_buffeer_[k][max_coordinate_h].z_buffer = -1;
            screen_buffeer_[k][min_coordinate_h].r = 1;
            screen_buffeer_[k][min_coordinate_h].z_buffer = -1;
        }

        for (int k = min_coordinate_h; k < max_coordinate_h; k++) {
            screen_buffeer_[min_coordinate_w][k].r = 1;
            screen_buffeer_[min_coordinate_w][k].z_buffer = -1;
            screen_buffeer_[max_coordinate_w][k].r = 1;
            screen_buffeer_[max_coordinate_w][k].z_buffer = -1;
        }*/
    }
#endif // DEBUG_3D


    for (int k = min_coordinate_w; k < max_coordinate_w; k++) {

        float x = (static_cast<float>(k) - WIDTH_SCREEN / 2) / (WIDTH_SCREEN / 2);

        for (int j = min_coordinate_h; j < max_coordinate_h; j++) {
            float y = (static_cast<float>(j) - HIGHT_SCREEN / 2) / (HIGHT_SCREEN / 2);
            float z = (Vector3::Dot(N0, second_vertex) -
                N0.GetX() * x - N0.GetY() * y) / N0.GetZ();
            Vector3 Q(x, y, z);
            if (!ThisPixelLyingInTringle(first_vertex, second_vertex, third_vertex, k,  j)) {
                continue;
            }
            float z_view_space = 2 * near_plate_ / (z - 1);
            Vector3 Q_view_space = inversed_frustrum_matrix.ProductOnVector3(Q * z_view_space, z_view_space);
            Vector3 Q_world_space = transform_matrix_camera_to_world_.ProductOnVector3(Q_view_space, 1);
            if (screen_buffeer_[k][j].z_buffer > z) {
#ifdef DEBUG_DEEP
                screen_buffeer_[k][j].r = (1 + z) / 2;
                screen_buffeer_[k][j].g = (1 + z) / 2;
                screen_buffeer_[k][j].b = (1 + z) / 2;
                screen_buffeer_[k][j].z_buffer = z;
#else
                Color color_pixel = PixelShading(property_poligon, transform_matrix_camera_to_world_.ProductOnVector3ForVectors(normal), Q_world_space);
                screen_buffeer_[k][j].r = color_pixel.GetR();
                screen_buffeer_[k][j].g = color_pixel.GetG();
                screen_buffeer_[k][j].b = color_pixel.GetB();
                screen_buffeer_[k][j].z_buffer = z;
#endif // DEBUG_DEEP
            }
        }
    }
}

float Shader::FindMax(const float& first_value, const float& second_value, const float& third_value)
{
    float max_val = first_value;

    if (second_value > max_val) {
        max_val = second_value;
    }

    if (third_value > max_val) {
        max_val = third_value;
    }

    return max_val;
}

float Shader::FindMin(const float& first_value, const float& second_value, const float& third_value)
{
    float min_val = first_value;

    if (second_value < min_val) {
        min_val = second_value;
    }

    if (third_value < min_val) {
        min_val = third_value;
    }

    return min_val;
}

bool Shader::ThisPixelLyingInTringle(const Vector3& first_vertex, const Vector3& second_vertex, const Vector3& third_vertex, const int& x0, const int& y0) const
{
    int x1 = static_cast<int>(first_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
    int y1 = static_cast<int>(first_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);

    int x2 = static_cast<int>(second_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
    int y2 = static_cast<int>(second_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);

    int x3 = static_cast<int>(third_vertex.GetX() * WIDTH_SCREEN / 2 + WIDTH_SCREEN / 2);
    int y3 = static_cast<int>(third_vertex.GetY() * HIGHT_SCREEN / 2 + HIGHT_SCREEN / 2);

    int a = (x1 - x0) * (y2 - y1) - (x2 - x1) * (y1 - y0);
    int b = (x2 - x0) * (y3 - y2) - (x3 - x2) * (y2 - y0);
    int c = (x3 - x0) * (y1 - y3) - (x1 - x3) * (y3 - y0);

    return ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0));
}

bool Shader::ClipingTestNearPlate(const Vector3& vertex)
{
    return vertex.GetZ() < -near_plate_;
}

Color Shader::LightShading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q)  const
{
	Color ans_color(0.0f, 0.0f, 0.0f);

	for_each(light_sources_->begin(), light_sources_->end(), [&property_poligon, &normal, &Q, &ans_color](auto& it) {
		ans_color += it->Shading(property_poligon, normal, Q);
	});

	return ans_color;
}
