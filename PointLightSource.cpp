#include "PointLightSource.h"

PointLightSource::PointLightSource()
{

}

PointLightSource::PointLightSource(const Color& light_color, const Vector3& position)
{
    color_of_light_ = light_color;
    position_ = position;
}

Color PointLightSource::Shading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const
{
    Color ans_color;

    for (auto&& it : shadows_) {
        if (LyingInsideShadowVoluem(it, Q)) {
            return ans_color;
        }
    }

    Vector3 direction_vector = position_ - Q;
    direction_vector.ToNormalize();
    ans_color = color_of_light_ * property_poligon.diffuse_reflection_color * fmaxf(Vector3::Dot(direction_vector, normal), 0);

    return ans_color;
}

void PointLightSource::CreateShadowVolumes(std::vector<Shape>* list_objects)
{
    shadows_.clear();
    for (auto it = list_objects->begin(); it < list_objects->end(); ++it) {
        if (!(it->GetNeedDynamicShadow())) {
            continue;
        }

        std::vector<Edge> shadow;
        for (int i = 0; i < it->GetNumPoligons(); i++) {
            for (int j = i + 1; j < it->GetNumPoligons(); j++) {
                if (IsPossitiveSide(it->GetPoligon(i).GetNormal(), it->GetVertex(it->GetPoligon(i).GetVertexIndex(0)).GetPosition()) !=
                    IsPossitiveSide(it->GetPoligon(j).GetNormal(), it->GetVertex(it->GetPoligon(j).GetVertexIndex(0)).GetPosition())) {
                    for (int k = 0; k < 2; k++) {
                        for (int w = 0; w < 3; w++) {
                            if (it->GetPoligon(i).GetVertexIndex(k) == it->GetPoligon(j).GetVertexIndex(w)) {
                                if (it->GetPoligon(i).GetVertexIndex(k + 1) == it->GetPoligon(j).GetVertexIndex(w - 1)) {
                                    if (IsPossitiveSide(it->GetPoligon(i).GetNormal(), it->GetVertex(it->GetPoligon(i).GetVertexIndex(0)).GetPosition())) {
                                        shadow.push_back(CreateEdge(it->GetPoligon(i).GetNormal(),
                                            it->GetVertex(it->GetPoligon(i).GetVertexIndex(k)).GetPosition() - it->GetVertex(it->GetPoligon(i).GetVertexIndex(k + 1)).GetPosition(),
                                            (position_ - it->GetVertex(it->GetPoligon(i).GetVertexIndex(k)).GetPosition()),
                                            it->GetVertex(it->GetPoligon(i).GetVertexIndex(k)).GetPosition()));
                                    }
                                    else {
                                        shadow.push_back(CreateEdge(it->GetPoligon(j).GetNormal(),
                                            it->GetVertex(it->GetPoligon(j).GetVertexIndex(w - 1)).GetPosition() - it->GetVertex(it->GetPoligon(j).GetVertexIndex(w)).GetPosition(),
                                            (position_ - it->GetVertex(it->GetPoligon(j).GetVertexIndex(w)).GetPosition()),
                                            it->GetVertex(it->GetPoligon(j).GetVertexIndex(w)).GetPosition()));
                                    }

                                }

                            }
                        }
                    }
                }
            }
        }
        shadows_.push_back(shadow);
    }
}

bool PointLightSource::IsPossitiveSide(const Vector3& normal, const Vector3& point)
{
    Vector4 plate(normal, -Vector3::Dot(normal, point));

    return Vector4::Dot(plate, Vector4(position_, 1)) > 0.0f;
}
