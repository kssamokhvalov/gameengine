#include "Matrix3x3.h"

Matrix3x3 Matrix3x3::CreateZeroMatrix()
{
    Matrix3x3 ans_matrix;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            ans_matrix.matrix_[i][j] = 0;
        }
    }

    return ans_matrix;
}

Matrix3x3 Matrix3x3::CreateCovirianceMatrix(const std::vector<Vertex>& vertices, const Vector3& m)
{
    Matrix3x3 ans_matrix;

    std::for_each(vertices.begin(), vertices.end(), [&m, &ans_matrix](auto& it) {
        ans_matrix.matrix_[0][1] += (it.GetPosition().GetX() - m.GetX()) * (it.GetPosition().GetY() - m.GetY());
        ans_matrix.matrix_[0][2] += (it.GetPosition().GetX() - m.GetX()) * (it.GetPosition().GetZ() - m.GetZ());
        ans_matrix.matrix_[1][2] += (it.GetPosition().GetY() - m.GetY()) * (it.GetPosition().GetZ() - m.GetZ());

        ans_matrix.matrix_[0][0] += (it.GetPosition().GetX() - m.GetX()) * (it.GetPosition().GetX() - m.GetX());
        ans_matrix.matrix_[1][1] += (it.GetPosition().GetY() - m.GetY()) * (it.GetPosition().GetY() - m.GetY());
        ans_matrix.matrix_[2][2] += (it.GetPosition().GetZ() - m.GetZ()) * (it.GetPosition().GetZ() - m.GetZ());
    });

    ans_matrix.matrix_[1][0] = ans_matrix.matrix_[0][1];
    ans_matrix.matrix_[2][0] = ans_matrix.matrix_[0][2];
    ans_matrix.matrix_[2][1] = ans_matrix.matrix_[1][2];

    ans_matrix /= vertices.size();

    return ans_matrix;
}

void Matrix3x3::operator/=(const float& number)
{
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            matrix_[i][j] /= number;
        }
    }
}
