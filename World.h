#pragma once
#include <vector>
#include <algorithm>
#include "Shape.h"
#include "LightSource.h"
#include  "InfinitLightSource.h"
#include "PointLightSource.h"


class World
{
public:
	explicit World();

	void Update();
	std::vector<Shape>* GetListObject();
	std::vector<LightSource*>* GetListLightSources();
private:
	std::vector<Shape> list_objects_;
	std::vector<InfinitLightSource> infinit_light_sources_;
	std::vector<PointLightSource> point_light_sources_;
	std::vector<LightSource*> light_sources_;
};

