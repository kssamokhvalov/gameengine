#include "Screen.h"

void Screen::InitScreen()
{
    render_width_screen_ = WIDTH_SCREEN;
    render_hight_screen_ = HIGHT_SCREEN;


    glutInitWindowSize(render_width_screen_, render_hight_screen_);
    glutInitWindowPosition(10, 10);
    glutCreateWindow(NAME_WINDOW);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, render_width_screen_, 0.0, render_hight_screen_);

    width_screen_ = render_width_screen_;
    hight_screen_ = render_hight_screen_;

    glutReshapeFunc(Resize);
    glutDisplayFunc(Display);
    glutIdleFunc(update);
}

void Screen::Display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_POINTS);
    for (int i = 0; i < render_width_screen_; i++) {
        for (int k = 0; k < render_hight_screen_; k++) {
            glColor3f(screen_buffer_->at(i).at(k).r, screen_buffer_->at(i).at(k).g, screen_buffer_->at(i).at(k).b);
            ClearPixel(screen_buffer_->at(i).at(k));
            SetPixel(offset_width_screen_ + static_cast<int>(alpha_coeff_screen_ * i), static_cast<int>(alpha_coeff_screen_ * k));
        }
    }
    glEnd();
    glFlush();
}

void Screen::Update()
{
    glutPostRedisplay();
}

void Screen::Resize(int width, int height)
{
    width_screen_ = width;
    hight_screen_ = height;

    alpha_coeff_screen_ = hight_screen_ / render_hight_screen_;
    offset_width_screen_ = (width_screen_ - alpha_coeff_screen_ * render_width_screen_) / 2;

    glViewport(0, 0, width, height);
}

void Screen::SetScreenBuffer(std::vector<std::vector<Pixel>>* screen_buffer)
{
    screen_buffer_ = screen_buffer;
}

void Screen::SetPixel(const GLint& x, const GLint& y)
{
    glVertex2i(x, y);
    
}

void Screen::ClearPixel(Pixel& pixel)
{
#ifdef DEBUG_DEEP
    pixel.r = 1.0f;
    pixel.g = 1.0f;
    pixel.b = 1.0f;
    pixel.z_buffer = 1.1f;
#else
    pixel.r = 0.0f;
    pixel.g = 0.0f;
    pixel.b = 0.0f;
    pixel.z_buffer = 1.1f;
#endif // DEBUG_DEEP
}



