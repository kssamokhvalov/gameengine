#pragma once
class Color
{
public:
	Color();
	Color(const float& r, const float& g, const float& b);

	Color operator*(const Color& color) const;
	Color operator+(const Color& color) const;

	void operator+=(const Color& color);
	void operator*=(const Color& color);

	Color operator*(const float& value) const;
	Color operator+(const float& value) const;

	void operator=(const Color& color);

	float GetR() const;
	float GetG() const;
	float GetB() const;

	void SetR(const float& r);
	void SetG(const float& g);
	void SetB(const float& b);

private:
	float r_;
	float g_;
	float b_;
};

