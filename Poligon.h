#pragma once
#include<iostream>
#include "Math/Vector3.h"
#include "Color.h"
#include "Settings.h"

struct PropertyOfPoligon
{
	Color diffuse_reflection_color;
	Color specular_reflection_color;
	float specular_exponent = 0.0f;
};

class Poligon
{
public:
	//explicit Poligon(int first_vertex_index, int second_vertex_index, int third_vertex_index);
	explicit Poligon(const int& first_vertex_index, const int& second_vertex_index, const int& third_vertex_index, const PropertyOfPoligon& visual_property);
	
	int GetVertexIndex(const int& num_vertex) const;
	int GetFirstVertexIndex() const;
	int GetSecondVertexIndex() const;
	int GetThirdVertexIndex() const;

	void SetVisualProperty(const PropertyOfPoligon& visual_property);
	const PropertyOfPoligon& GetVisualProperty() const;

	void SetNormal(const Vector3& normal);
	const Vector3& GetNormal() const;

private:
	int  vertex_index_[3];  //because polygon is triangle
	Vector3 normal_;
	PropertyOfPoligon property_poligon_;
};

