#include "BoundingVolume.h"

BoundingVolume::BoundingVolume(const std::vector<Vertex>& vertices)
{
	Vector3 m(0, 0, 0);
	std::for_each(vertices.begin(), vertices.end(), [&m](auto& it) {
		m += it.GetPosition();
	});
	m /= vertices.size();
}
