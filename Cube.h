#pragma once
#include "Shape.h"
#include <vector>
class Cube :
    public Shape
{
public:
    explicit Cube();
    const Vertex& GetVertex(const int& index) const;
    const Poligon& GetPoligon(const int& index) const;
    const Vector3& GetPosition() const;
    void SetPosition(const Vector3& position);
    const int& GetNumPoligons() const;
private:
    std::vector<Vertex> vertices_;
    std::vector<Poligon> poligons_;
    Vector3 position_;
    float length_a = 1;
};

