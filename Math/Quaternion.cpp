#include"Quaternion.h"

Quaternion::Quaternion()
{
	w_ = 0.0f;
	i_ = 0.0f;
	j_ = 0.0f;
	k_ = 0.0f;
}

Quaternion::Quaternion(const float& w, const float& i, const float& j, const float& k)
{
	w_ = w;
	i_ = i;
	j_ = j;
	k_ = k;
}

Quaternion::Quaternion(const Vector3& A, const float& angle_in_radians)
{
	w_ = cosf(angle_in_radians / 2);
	i_ = A.GetX() * sinf(angle_in_radians / 2);
	j_ = A.GetY() * sinf(angle_in_radians / 2);
	k_ = A.GetZ() * sinf(angle_in_radians / 2);
}

void Quaternion::operator=(const Quaternion& quaterion)
{
	w_ = quaterion.w_;
	i_ = quaterion.i_;
	j_ = quaterion.j_;
	k_ = quaterion.k_;
}

Quaternion Quaternion::operator+(const Quaternion& quaterion) const
{
	return Quaternion(w_ + quaterion.w_, i_ + quaterion.i_, j_ + quaterion.j_, k_ + quaterion.k_);
}

Quaternion Quaternion::operator+(const float& number) const
{
	return Quaternion(w_ + number, i_ + number, j_ + number, k_ + number);
}

Quaternion& Quaternion::operator+=(const Quaternion& quaterion)
{
	w_ + quaterion.w_;
	i_ + quaterion.i_;
	j_ + quaterion.j_;
	k_ + quaterion.k_;
	return *this;
}

Quaternion& Quaternion::operator+=(const float& number)
{
	w_ + number;
	i_ + number;
	j_ + number;
	k_ + number;
	return *this;
}

Quaternion Quaternion::operator-(const Quaternion& quaterion) const
{
	return Quaternion(w_ - quaterion.w_, i_ - quaterion.i_, j_ - quaterion.j_, k_ - quaterion.k_);
}

Quaternion Quaternion::operator-(const float& number) const
{
	return Quaternion(w_ - number, i_ - number, j_ - number, k_ - number);
}

Quaternion& Quaternion::operator-=(const Quaternion& quaterion)
{
	w_ - quaterion.w_;
	i_ - quaterion.i_;
	j_ - quaterion.j_;
	k_ - quaterion.k_;
	return *this;
}

Quaternion& Quaternion::operator-=(const float& number)
{
	w_ - number;
	i_ - number;
	j_ - number;
	k_ - number;
	return *this;
}

Quaternion Quaternion::operator*(const Quaternion& quaterion) const
{
	return Quaternion(w_ * quaterion.w_ - i_ * quaterion.i_ - j_ * quaterion.j_ - k_ * quaterion.k_,
					  w_ * quaterion.i_ + i_ * quaterion.w_ + j_ * quaterion.k_ - k_ * quaterion.j_,
					  w_ * quaterion.j_ - i_ * quaterion.k_ + j_ * quaterion.w_ + k_ * quaterion.i_,
					  w_ * quaterion.k_ + i_ * quaterion.j_ - j_ * quaterion.i_ + k_ * quaterion.w_);
}

Quaternion Quaternion::operator*(const float& number) const
{
	return Quaternion(w_ * number, i_ * number, j_ * number, k_ * number);
}

Quaternion& Quaternion::operator*=(const Quaternion& quaterion)
{
	float w = w_ * quaterion.w_ - i_ * quaterion.i_ - j_ * quaterion.j_ - k_ * quaterion.k_;
	float i = w_ * quaterion.i_ + i_ * quaterion.w_ + j_ * quaterion.k_ - k_ * quaterion.j_;
	float j = w_ * quaterion.j_ - i_ * quaterion.k_ + j_ * quaterion.w_ + k_ * quaterion.i_;
	float k = w_ * quaterion.k_ + i_ * quaterion.j_ - j_ * quaterion.i_ + k_ * quaterion.w_;

	w_ = w;
	i_ = i;
	j_ = j;
	k_ = k;

	return *this;
}

Quaternion& Quaternion::operator*=(const float& number)
{
	w_ * number;
	i_ * number;
	j_ * number;
	k_ * number;
	return *this;
}

Quaternion Quaternion::operator/(const Quaternion& quaterion) const
{
	return *this * ToInverse(quaterion);
}

Quaternion Quaternion::operator/(const float& number) const
{
	return Quaternion(w_ / number, i_ / number, j_ / number, k_ / number);
}

Quaternion& Quaternion::operator/=(const Quaternion& quaterion)
{
	*this *= ToInverse(quaterion);
	return *this;
}

Quaternion& Quaternion::operator/=(const float& number)
{
	w_ / number;
	i_ / number;
	j_ / number;
	k_ / number;
	return *this;
}

Quaternion Quaternion::ToInverse(const Quaternion& quaterion)
{
	return ToConjugate(quaterion) / quaterion.GetSquareMagnitude();
}

Quaternion Quaternion::ToConjugate(const Quaternion& quaterion)
{
	return Quaternion(quaterion.w_, -quaterion.i_, -quaterion.j_, -quaterion.k_);
}

void Quaternion::ToInverse()
{
	*this = ToConjugate(*this) / GetSquareMagnitude();
}

void Quaternion::ToConjugate()
{
	i_ = -i_;
	j_ = -j_;
	k_ = -k_;
}

float Quaternion::GetSquareMagnitude() const
{
	return w_ * w_ + i_ * i_ + j_ * j_ + k_ * k_;
}

float Quaternion::GetW() const
{
	return w_;
}

float Quaternion::GetI() const
{
	return i_;
}

float Quaternion::GetJ() const
{
	return j_;
}

float Quaternion::GetK() const
{
	return k_;
}
