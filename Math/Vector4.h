#pragma once
#include <math.h>
#include <float.h>
#include "Vector3.h"

class Vector4 {

public:
	explicit Vector4();
	explicit Vector4(const Vector3& vector3, const float& w);
	explicit Vector4(const float& x, const float& y, const float& z, const float& w);

	Vector4 operator+(const Vector4& vector4) const;
	Vector4 operator+(const float& number) const;
	Vector4& operator+=(const Vector4& vector4);
	Vector4& operator+=(const float& number);

	Vector4 operator-();
	Vector4 operator-(const Vector4& vector4) const;
	Vector4 operator-(const float& number) const;
	Vector4& operator-=(const Vector4& vector4);
	Vector4& operator-=(const float& number);

	Vector4 operator*(const float& number) const;
	Vector4& operator*=(const float& number);

	Vector4 operator/(const float& number) const;
	Vector4& operator/=(const float& number);

	static float Dot(const Vector4& firstVector4, const Vector4& secondVector4);

	float GetMagnitude() const;
	float GetSquareMagnitude() const;
	float GetX() const;
	float GetY() const;
	float GetZ() const;
	float GetW() const;
	void ToNormalize();

private:

	float x_;
	float y_;
	float z_;
	float w_;
};