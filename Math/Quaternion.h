#pragma once
#include"Vector3.h"


class Quaternion {

public:
	Quaternion();
	explicit Quaternion(const float& w, const float& i, const float& j, const float& k);
	explicit Quaternion(const Vector3& A, const float& angle_in_radians);

	void operator=(const Quaternion& quaterion);

	Quaternion operator+(const Quaternion& quaterion) const;
	Quaternion operator+(const float& number) const;
	Quaternion& operator+=(const Quaternion& quaterion);
	Quaternion& operator+=(const float& number);

	Quaternion operator-(const Quaternion& quaterion) const;
	Quaternion operator-(const float& number) const;
	Quaternion& operator-=(const Quaternion& quaterion);
	Quaternion& operator-=(const float& number);

	Quaternion operator*(const Quaternion& quaterion) const;
	Quaternion operator*(const float& number) const;
	Quaternion& operator*=(const Quaternion& quaterion);
	Quaternion& operator*=(const float& number);

	Quaternion operator/(const Quaternion& quaterion) const;
	Quaternion operator/(const float& number) const;
	Quaternion& operator/=(const Quaternion& quaterion);
	Quaternion& operator/=(const float& number);

	static Quaternion ToInverse(const Quaternion& quaterion);
	static Quaternion ToConjugate(const Quaternion& quaterion);

	void ToInverse();
	void ToConjugate();
	float GetSquareMagnitude() const;

	float GetW() const;
	float GetI() const;
	float GetJ() const;
	float GetK() const;

private:

	float w_;
	float i_;
	float j_;
	float k_;
};