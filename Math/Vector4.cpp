#include "Vector4.h"

Vector4::Vector4()
{
	x_ = 0;
	y_ = 0;
	z_ = 0;
	w_ = 0;
}

Vector4::Vector4(const Vector3& vector3, const float& w)
{
	x_ = vector3.GetX();
	y_ = vector3.GetY();
	z_ = vector3.GetZ();
	w_ = w;
}

Vector4::Vector4(float const& x, float const& y, float const& z, float const& w)
{
	x_ = x;
	y_ = y;
	z_ = z;
	w_ = w;
}

Vector4 Vector4::operator+(const Vector4& vector4) const
{
	return Vector4(x_ + vector4.x_, y_ + vector4.y_, z_ + vector4.z_, w_ + vector4.w_);
}

Vector4 Vector4::operator+(const float& number) const
{
	return Vector4(x_ + number, y_ + number, z_ + number, w_ + number);
}

Vector4& Vector4::operator+=(const Vector4& Vector4)
{
	x_ += Vector4.x_;
	y_ += Vector4.y_;
	z_ += Vector4.z_;

	return *this;
}

Vector4& Vector4::operator+=(const float& number)
{
	x_ += number;
	y_ += number;
	z_ += number;

	return *this;
}

Vector4 Vector4::operator-()
{
	return Vector4(-x_, -y_, -z_, -w_);
}

Vector4 Vector4::operator-(const Vector4& vector4) const
{
	return Vector4(x_ - vector4.x_, y_ - vector4.y_, z_ - vector4.z_, w_ - vector4.w_);
}

Vector4 Vector4::operator-(const float& number) const
{
	return Vector4(x_ - number, y_ - number, z_ - number, w_ - number);
}

Vector4& Vector4::operator-=(const Vector4& vector4)
{
	x_ -= vector4.x_;
	y_ -= vector4.y_;
	z_ -= vector4.z_;

	return *this;
}

Vector4& Vector4::operator-=(const float& number)
{
	x_ -= number;
	y_ -= number;
	z_ -= number;

	return *this;
}

Vector4 Vector4::operator*(const float& number) const
{
	return Vector4(x_ * number, y_ * number, z_ * number, w_ * number);
}

Vector4& Vector4::operator*=(const float& number)
{
	x_ *= number;
	y_ *= number;
	z_ *= number;

	return *this;
}

Vector4 Vector4::operator/(const float& number) const
{
	return Vector4(x_ / number, y_ / number, z_ / number, w_ /number);
}

Vector4& Vector4::operator/=(const float& number)
{
	x_ /= number;
	y_ /= number;
	z_ /= number;

	return *this;
}

float Vector4::Dot(const Vector4& firstVector4, const Vector4& secondVector4)
{
	return firstVector4.x_ * secondVector4.x_ + firstVector4.y_ * secondVector4.y_ + firstVector4.z_ * secondVector4.z_ + firstVector4.w_ * secondVector4.w_;
}

float Vector4::GetMagnitude() const
{
	return sqrtf(x_ * x_ + y_ * y_ + z_ * z_ + w_ * w_);
}

float Vector4::GetSquareMagnitude() const
{
	return x_ * x_ + y_ * y_ + z_ * z_;
}

float Vector4::GetX() const
{
	return x_;
}

float Vector4::GetY() const
{
	return y_;
}

float Vector4::GetZ() const
{
	return z_;
}

float Vector4::GetW() const
{
	return w_;
}

void Vector4::ToNormalize()
{
	*this /= (GetMagnitude() + FLT_MIN);
}