#pragma once
#include <math.h>
#include <float.h>

class Vector3 {

public:
	explicit Vector3();
	explicit Vector3(const float& x, const float& y, const float& z);

	Vector3 operator+(const Vector3& vector3) const;
	Vector3 operator+(const float& number) const;
	void operator+=(const Vector3& vector3);
	Vector3& operator+=(const float& number);

	Vector3 operator-();
	Vector3 operator-(const Vector3& vector3) const;
	Vector3 operator-(const float& number) const;
	Vector3& operator-=(const Vector3& vector3);
	Vector3& operator-=(const float& number);

	Vector3 operator*(const float& number) const;
	Vector3& operator*=(const float& number);

	Vector3 operator/(const float& number) const;
	void operator/=(const float& number);

	static float Dot(const Vector3& firstVector3, const Vector3& secondVector3);
	static Vector3 Cross(const Vector3& firstVector3, const Vector3& secondVector3);

	void Cross(const Vector3& vector3);
	

	void SetX(const float& x);
	void SetY(const float& y);
	void SetZ(const float& z);
	float GetX() const;
	float GetY() const;
	float GetZ() const;
	float GetMagnitude() const;
	float GetSquareMagnitude() const;
	void ToNormalize();

private:

	float x_;
	float y_;
	float z_;
};
