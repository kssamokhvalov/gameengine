#include "Vector3.h"

Vector3::Vector3()
{
	x_ = 0;
	y_ = 0;
	z_ = 0;
}

Vector3::Vector3(const float& x, const float& y, const float& z)
{
	x_ = x;
	y_ = y;
	z_ = z;
}

Vector3 Vector3::operator+(const Vector3& vector3) const
{
	return Vector3(x_ + vector3.x_, y_ + vector3.y_, z_ + vector3.z_);
}

Vector3 Vector3::operator+(const float& number) const
{
	return Vector3(x_ + number, y_ + number, z_ + number);
}

void Vector3::operator+=(const Vector3& vector3)
{
	x_ += vector3.x_;
	y_ += vector3.y_;
	z_ += vector3.z_;
}

Vector3& Vector3::operator+=(const float& number)
{
	x_ += number;
	y_ += number;
	z_ += number;

	return *this;
}

Vector3 Vector3::operator-()
{
	return Vector3(-x_, -y_, -z_);
}

Vector3 Vector3::operator-(const Vector3& vector3) const
{
	return Vector3(x_ - vector3.x_, y_ - vector3.y_, z_ - vector3.z_);
}

Vector3 Vector3::operator-(const float& number) const
{
	return Vector3(x_ - number, y_ - number, z_ - number);
}

Vector3& Vector3::operator-=(const Vector3& vector3)
{
	x_ -= vector3.x_;
	y_ -= vector3.y_;
	z_ -= vector3.z_;

	return *this;
}

Vector3& Vector3::operator-=(const float& number)
{
	x_ -= number;
	y_ -= number;
	z_ -= number;

	return *this;
}

Vector3 Vector3::operator*(const float& number) const
{
	return Vector3(x_ * number, y_ * number, z_ * number);
}

Vector3& Vector3::operator*=(const float& number)
{
	x_ *= number;
	y_ *= number;
	z_ *= number;

	return *this;
}

Vector3 Vector3::operator/(const float& number) const
{
	return Vector3(x_ / number, y_ / number, z_ / number);
}

void Vector3::operator/=(const float& number)
{
	x_ /= number;
	y_ /= number;
	z_ /= number;
}

float Vector3::Dot(const Vector3& firstVector3, const Vector3& secondVector3)
{
	return firstVector3.x_ * secondVector3.x_ + firstVector3.y_ * secondVector3.y_ + firstVector3.z_ * secondVector3.z_;
}

void Vector3::Cross(const Vector3& vector3)
{
	x_ = y_ * vector3.z_ - z_ * vector3.y_;
	y_ = z_ * vector3.x_ - x_ * vector3.z_;
	z_ = x_ * vector3.y_ - y_ * vector3.x_;
}

void Vector3::SetX(const float& x)
{
	x_ = x;
}

void Vector3::SetY(const float& y)
{
	y_ = y;
}

void Vector3::SetZ(const float& z)
{
	z_ = z;
}

float Vector3::GetX() const
{
	return x_;
}

float Vector3::GetY() const
{
	return y_;
}

float Vector3::GetZ() const
{
	return z_;
}

Vector3 Vector3::Cross(const Vector3& firstVector3, const Vector3& secondVector3)
{
	return Vector3(firstVector3.y_ * secondVector3.z_ - firstVector3.z_ * secondVector3.y_,
		firstVector3.z_ * secondVector3.x_ - firstVector3.x_ * secondVector3.z_,
		firstVector3.x_ * secondVector3.y_ - firstVector3.y_ * secondVector3.x_);
}

float Vector3::GetMagnitude() const
{
	return sqrtf(x_ * x_ + y_ * y_ + z_ * z_);
}

float Vector3::GetSquareMagnitude() const
{
	return x_ * x_ + y_ * y_ + z_ * z_;
}

void Vector3::ToNormalize()
{
	*this /= (GetMagnitude() + FLT_MIN);
}
