#pragma once
#include "Math/Vector3.h"
#include "Vertex.h"
#include <vector>
#include <algorithm>

class BoundingVolume
{
public:
	explicit BoundingVolume(const std::vector<Vertex>& vertices);
private:
	Vector3 Q_;
	Vector3 R_;
	Vector3 S_;
	Vector3 T_;
};

