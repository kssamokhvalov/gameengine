#pragma once
#include <vector>
#include <algorithm>

#include "Color.h"
#include "Poligon.h"
#include "Math/Vector3.h"
#include "Matrix4x4.h"
#include "LightSource.h"
#include "Camera.h"

struct Pixel
{
	float r = 0;
	float g = 0;
	float b = 0;
	float z_buffer = -1;
};


class Shader
{
public:

	Shader();
	Shader(std::vector<LightSource*>* light_sources, const Color& embied_color);
	Color PixelShading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const;
	void CreateShadowVoluem(std::vector<Shape>* list_objects);
	void SetAmbiedColor(const Color& embied_color);
	void SetListLightSources(std::vector<LightSource*>* light_sources);
	void DrawObjects(std::vector<Shape>* list_objects, const Matrix4x4& transform_matrix_camera_to_world);
	void RenderObject(const Shape& shape, const Matrix4x4& rotation_matrix);
	std::vector<std::vector<Pixel>>* GetScreenBuffer();

private:
	Color LightShading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const;
	void Rastirase(const Vector3& first_vertex, const Vector3& second_vertex, const Vector3& third_vertex, const Vector3& normal, const PropertyOfPoligon& property_poligon);
	float FindMax(const float& first_value, const float& second_value, const float& third_value);
	float FindMin(const float& first_value, const float& second_value, const float& third_value);
	bool ThisPixelLyingInTringle(const Vector3& first_vertex, const Vector3& second_vertex, const Vector3& third_vertex, const int& x0, const int& y0) const;
	bool ClipingTestNearPlate(const Vector3& vertex);

	std::vector<std::vector<Pixel>> screen_buffeer_;

	std::vector<LightSource*> *light_sources_;
	Color embied_color_;
	Matrix4x4 transform_matrix_camera_to_world_;
	Matrix4x4 transform_matrix_world_to_camera_;
	Vector3 viewer_vector_;
	float accses_draw;

	Vector3 N_near_plate_;
	Vector4 top_plate_;
	Vector4 bottom_plate_;

	Matrix4x4 frustrum_matrix;
	Matrix4x4 inversed_frustrum_matrix;
	float near_plate_;
};

