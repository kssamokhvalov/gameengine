#pragma once
#include <vector>
#include "Vertex.h"
#include "Poligon.h"
#include "Math/Quaternion.h"
#include "Matrix4x4.h"
#include "BoundingVolume.h"

class Shape
{
public:
    Shape();
    static Shape CreateCube(const Vector3& position, const Quaternion& oritation, const float& edge_length);
    static Shape CreatePoligon();

    const Vertex& GetVertex(const int& index) const;
    const Poligon& GetPoligon(const int& index) const;
    const Vector3& GetPosition() const;
    void SetPosition(const Vector3& position);
    const Quaternion& GetOritation() const;
    void SetOritation(const Quaternion& oritation);
    Matrix4x4 GetTransformationMatrixToWorld() const;
    void Action();
    const int& GetNumPoligons() const;
    Shape GetWorldObject();

    void SetNeedDynamicShadow(const bool& stait);
    bool GetNeedDynamicShadow();
private:
    std::vector<Vertex> vertices_;
    std::vector<Poligon> poligons_;
    Vector3 position_;
    Quaternion oritation_;
    bool need_dynamic_shadow_;
};

