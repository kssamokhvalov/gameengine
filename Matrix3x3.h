#pragma once
#include "Vertex.h"
#include "Math/Vector3.h"
#include <vector>
#include <algorithm>

class Matrix3x3
{
public:
	static Matrix3x3 CreateZeroMatrix();
	static Matrix3x3 CreateCovirianceMatrix(const std::vector<Vertex>& vertices, const Vector3& m);

	void operator/= (const float& number);
private:
	float matrix_[3][3];
};

