#include "Color.h"

Color::Color()
{
	r_ = 0.0f;
	g_ = 0.0f;
	b_ = 0.0f;
}

Color::Color(const float& r, const float& g, const float& b)
{
	r_ = r;
	g_ = g;
	b_ = b;
}

Color Color::operator*(const Color& color) const
{
	Color ans_color;

	ans_color.r_ = r_ * color.r_;
	ans_color.g_ = g_ * color.g_;
	ans_color.b_ = b_ * color.b_;

	return ans_color;
}

Color Color::operator+(const Color& color) const
{
	Color ans_color;

	ans_color.r_ = r_ + color.r_;
	ans_color.g_ = g_ + color.g_;
	ans_color.b_ = b_ + color.b_;

	return ans_color;
}

void Color::operator+=(const Color& color)
{
	r_ += color.r_;
	g_ += color.g_;
	b_ += color.b_;
}

void Color::operator*=(const Color& color)
{
	r_ *= color.r_;
	g_ *= color.g_;
	b_ *= color.b_;
}

Color Color::operator*(const float& value) const
{
	Color ans_color;

	ans_color.r_ = r_ * value;
	ans_color.g_ = g_ * value;
	ans_color.b_ = b_ * value;
	
	return ans_color;
}

Color Color::operator+(const float& value) const
{
	Color ans_color;

	ans_color.r_ = r_ + value;
	ans_color.g_ = g_ + value;
	ans_color.b_ = b_ + value;

	return ans_color;
}

void Color::operator=(const Color& color)
{
	r_ = color.r_;
	g_ = color.g_;
	b_ = color.b_;
}

float Color::GetR() const
{
	return r_;
}

float Color::GetG() const
{
	return g_;
}

float Color::GetB() const
{
	return b_;
}

void Color::SetR(const float& r)
{
	r_ = r;
}

void Color::SetG(const float& g)
{
	g_ = g;
}

void Color::SetB(const float& b)
{
	b_ = b;
}
