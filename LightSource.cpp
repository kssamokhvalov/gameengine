#include "LightSource.h"

Edge LightSource::CreateEdge(const Vector3& normal_poligon, const Vector3& direct_edge, const Vector3& direct_light, const Vector3& any_vertex_position)
{
    Edge edge;

    edge.top_plate_ = Vector4(normal_poligon, -Vector3::Dot(normal_poligon, any_vertex_position));

    Vector3 normal_edge_plate = Vector3::Cross(direct_light, direct_edge);
    edge.edge_plate_ = Vector4(normal_edge_plate, -Vector3::Dot(normal_edge_plate, any_vertex_position));

    return edge;
}

bool LightSource::LyingInsideShadowVoluem(const std::vector<Edge>& voluem, const Vector3& point) const
{
    if (voluem.empty()) {
        return false;
    }
    for (auto it = voluem.begin(); it < voluem.end(); ++it) {
        if (Vector4::Dot(it->edge_plate_, Vector4(point, 1)) > -0.01 || Vector4::Dot(it->top_plate_, Vector4(point, 1)) > -0.01)
            return false;
    }
    return true;
}
