#include "Shape.h"

Shape::Shape()
{
}

Shape Shape::CreateCube(const Vector3& position, const Quaternion& oritation, const float& edge_length)
{
	Shape cube;
	cube.vertices_.push_back(Vertex(-edge_length / 2, -edge_length / 2, -edge_length / 2));
	cube.vertices_.push_back(Vertex(edge_length / 2, -edge_length / 2, -edge_length / 2));
	cube.vertices_.push_back(Vertex(edge_length / 2, edge_length / 2, -edge_length / 2));
	cube.vertices_.push_back(Vertex(-edge_length / 2, edge_length / 2, -edge_length / 2));
	cube.vertices_.push_back(Vertex(-edge_length / 2, -edge_length / 2, edge_length / 2));
	cube.vertices_.push_back(Vertex(edge_length / 2, -edge_length / 2, edge_length / 2));
	cube.vertices_.push_back(Vertex(edge_length / 2, edge_length / 2, edge_length / 2));
	cube.vertices_.push_back(Vertex(-edge_length / 2, edge_length / 2, edge_length / 2));

	Color cube_color(0.5f, 0.5f, 0.5f);
	PropertyOfPoligon visual_property_cube;
	visual_property_cube.diffuse_reflection_color = cube_color;
	visual_property_cube.specular_exponent = 0;
	visual_property_cube.specular_reflection_color = cube_color;

	cube.poligons_.push_back(Poligon(0, 2, 1, visual_property_cube));
	cube.poligons_.push_back(Poligon(0, 3, 2, visual_property_cube));

	cube.poligons_.push_back(Poligon(0, 1, 5, visual_property_cube));
	cube.poligons_.push_back(Poligon(0, 5, 4, visual_property_cube));

	cube.poligons_.push_back(Poligon(1, 2, 6, visual_property_cube));
	cube.poligons_.push_back(Poligon(1, 6, 5, visual_property_cube));

	cube.poligons_.push_back(Poligon(2, 3, 7, visual_property_cube));
	cube.poligons_.push_back(Poligon(2, 7, 6, visual_property_cube));

	cube.poligons_.push_back(Poligon(3, 0, 4, visual_property_cube));
	cube.poligons_.push_back(Poligon(3, 4, 7, visual_property_cube));

	cube.poligons_.push_back(Poligon(4, 5, 6, visual_property_cube));
	cube.poligons_.push_back(Poligon(4, 6, 7, visual_property_cube));

	for (auto it = std::begin(cube.poligons_); it != std::end(cube.poligons_); ++it) {
		Vector3 normal = Vector3::Cross((cube.vertices_.at(it->GetSecondVertexIndex()).GetPosition() - cube.vertices_.at(it->GetFirstVertexIndex()).GetPosition()),
			(cube.vertices_.at(it->GetThirdVertexIndex()).GetPosition() - cube.vertices_.at(it->GetFirstVertexIndex()).GetPosition()));
		normal.ToNormalize();
		it->SetNormal(normal);
	}
	cube.position_ = position;
	cube.oritation_ = oritation;

	BoundingVolume box(cube.vertices_);
	cube.need_dynamic_shadow_ = true;

	return cube;
}

Shape Shape::CreatePoligon()
{
	Shape poligon;

	poligon.vertices_.push_back(Vertex(-10, -1, 10));
	poligon.vertices_.push_back(Vertex(0, -1, 10));
	poligon.vertices_.push_back(Vertex(10, -1, 10));
	

	Color cube_color(0.5f, 0.5f, 0.5f);
	PropertyOfPoligon visual_property_cube;
	visual_property_cube.diffuse_reflection_color = cube_color;
	visual_property_cube.specular_exponent = 0;
	visual_property_cube.specular_reflection_color = cube_color;

	poligon.poligons_.push_back(Poligon(0, 2, 1, visual_property_cube));

	for (auto it = std::begin(poligon.poligons_); it != std::end(poligon.poligons_); ++it) {
		Vector3 normal = Vector3::Cross((poligon.vertices_.at(it->GetSecondVertexIndex()).GetPosition() - poligon.vertices_.at(it->GetFirstVertexIndex()).GetPosition()),
			(poligon.vertices_.at(it->GetThirdVertexIndex()).GetPosition() - poligon.vertices_.at(it->GetFirstVertexIndex()).GetPosition()));
		normal.ToNormalize();
		it->SetNormal(normal);
	}

	poligon.position_ = Vector3(0, 0, 0);
	poligon.oritation_ = Quaternion(1, 0, 0, 0);

	return poligon;
}

const Vertex& Shape::GetVertex(const int& index) const
{
	return vertices_.at(index);
}

const Poligon& Shape::GetPoligon(const int& index) const
{
	return poligons_.at(index);
}

const Vector3& Shape::GetPosition() const
{
	return position_;
}

void Shape::SetPosition(const Vector3& position)
{
	position_ = position;
}

const Quaternion& Shape::GetOritation() const
{
	return  oritation_;
}

void Shape::SetOritation(const Quaternion& oritation)
{
	oritation_ = oritation;
}

Matrix4x4 Shape::GetTransformationMatrixToWorld() const
{
	return Matrix4x4::CreateTransformationMatrix(oritation_, position_);
}

void Shape::Action()
{
	Vector3 vector_axes_rotation(1, 1, 0);
	vector_axes_rotation.ToNormalize();
	Quaternion rotation(vector_axes_rotation, 0.0);
	oritation_ *= rotation;
}

const int& Shape::GetNumPoligons() const
{
	return poligons_.size();
}

Shape Shape::GetWorldObject()
{
	Shape ans;
	Matrix4x4 transform_object_to_world = GetTransformationMatrixToWorld();

	for (auto it = vertices_.begin(); it < vertices_.end(); ++it) {
		ans.vertices_.push_back(transform_object_to_world.ProductOnVector3(it->GetPosition(), 1));
	}

	for (auto it = std::begin(ans.poligons_); it != std::end(ans.poligons_); ++it) {
		Vector3 normal = Vector3::Cross((ans.vertices_.at(it->GetSecondVertexIndex()).GetPosition() - ans.vertices_.at(it->GetFirstVertexIndex()).GetPosition()),
			(ans.vertices_.at(it->GetThirdVertexIndex()).GetPosition() - ans.vertices_.at(it->GetFirstVertexIndex()).GetPosition()));
		normal.ToNormalize();
		it->SetNormal(normal);
	}

	ans.poligons_ = poligons_;
	ans.position_ = position_;
	ans.oritation_ = oritation_;
	ans.need_dynamic_shadow_ = need_dynamic_shadow_;

	return ans;
}

void Shape::SetNeedDynamicShadow(const bool& stait)
{
	need_dynamic_shadow_ = stait;
}

bool Shape::GetNeedDynamicShadow()
{
	return need_dynamic_shadow_;
}
