#pragma once
#include "Math/Vector4.h"
#include "Math/Vector3.h"
#include "Math/Quaternion.h"
#include <math.h>

class Matrix4x4
{
public:
	static Matrix4x4 CreateZeroMatrix();
	static Matrix4x4 CreateOnesMatrix();
	static Matrix4x4 CreateFrustumProjectionMatrix(const float& angle_FOV, const float& aspect, const float& near_plate, const float& far_plate);
	static Matrix4x4 CreateRotationMatrix(const Vector3& axis_A, const float& angle_radian);
	static Matrix4x4 CreateRotationMatrix(const Quaternion& quaternion);
	static Matrix4x4 CreateTransformationMatrix(const Vector3& axis_A, const float& angle_radian, const Vector3& vector_T);
	static Matrix4x4 CreateTransformationMatrix(const Quaternion& quaternion, const Vector3& vector_T);
	static Matrix4x4 CreateTransformationMatrix(const Matrix4x4& rotation_matrix, const Vector3& vector_T);

	Vector4 operator*(const Vector4& vector) const;
	Matrix4x4 operator*(const Matrix4x4& input_matrix) const;
	void operator*=(const Matrix4x4& input_matrix);
	void operator/=(const float& num);
	void operator=(const Matrix4x4& input_matrix);

	Vector3 ProductOnVector3(const Vector3& vector, const float& vector_w) const;
	Vector3 ProductOnVector3ForVectors(const Vector3& vector) const; // this hack, best create matrix 3x3 for rotation matrix(issue 2)
	float GetDeterminant() const;
	void InverseMatrix();
	Matrix4x4 GetInversedMatrix() const;
	void ChangeHandedness();

	static Matrix4x4 GetMatrixInverses(const Matrix4x4& matrix);
private:
	float matrix_[4][4];
};

