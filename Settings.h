#pragma once

#define WIDTH_SCREEN 640
#define	HIGHT_SCREEN 480
#define ASPECT 0.75f //for resolution 640x480
#define NAME_WINDOW "Game Engine"

//#define DEBUG_3D
//#define DEBUG_DEEP
#define ASSERT_ENABLE

enum Keyboard
{
	W = 'w',
	A = 'a',
	S = 's',
	D = 'd',
	I = 'i',
	J = 'j',
	K = 'k',
	L = 'l'
};
