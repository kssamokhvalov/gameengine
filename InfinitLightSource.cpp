#include "InfinitLightSource.h"

InfinitLightSource::InfinitLightSource()
{
    color_of_light_ = Color(0.0f, 0.0f, 0.0f);
    direction_vector_ = Vector3(0.0f, 0.0f, 0.0f);
}

InfinitLightSource::InfinitLightSource(const Color& light_color, const Vector3& direction_vector)
{
    color_of_light_ = light_color;
    direction_vector_ = direction_vector;
}

Color InfinitLightSource::Shading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const
{
    Color ans_color;

    for (auto&& it : shadows_) {
        if (LyingInsideShadowVoluem(it, Q)) {
            return ans_color;
        }
    }

    ans_color = color_of_light_ * property_poligon.diffuse_reflection_color * fmaxf(Vector3::Dot(direction_vector_, normal), 0);
    return ans_color;
}

void InfinitLightSource::CreateShadowVolumes(std::vector<Shape>* list_objects)
{
    shadows_.clear();

    for (auto it = list_objects->begin(); it < list_objects->end(); ++it) {
        if (!(it->GetNeedDynamicShadow())) {
            continue;
        }

        std::vector<Edge> shadow;
        for (int i = 0; i < it->GetNumPoligons(); i++) {
            for (int j = i + 1; j < it->GetNumPoligons(); j++) {
                if ((Vector3::Dot(it->GetPoligon(i).GetNormal(), direction_vector_) > 0) !=
                    (Vector3::Dot(it->GetPoligon(j).GetNormal(), direction_vector_) > 0)) {
                    for (int k = 0; k < 2; k++) {
                        for (int w = 0; w < 3; w++) {
                            if (it->GetPoligon(i).GetVertexIndex(k) == it->GetPoligon(j).GetVertexIndex(w)) {
                                if (it->GetPoligon(i).GetVertexIndex(k + 1) == it->GetPoligon(j).GetVertexIndex(w - 1)) {
                                    if (Vector3::Dot(it->GetPoligon(i).GetNormal(), direction_vector_) > 0) {
                                        shadow.push_back(CreateEdge(it->GetPoligon(i).GetNormal(),
                                            it->GetVertex(it->GetPoligon(i).GetVertexIndex(k)).GetPosition() - it->GetVertex(it->GetPoligon(i).GetVertexIndex(k + 1)).GetPosition(),
                                            direction_vector_,
                                            it->GetVertex(it->GetPoligon(i).GetVertexIndex(k)).GetPosition()));
                                    }
                                    else {
                                        shadow.push_back(CreateEdge(it->GetPoligon(j).GetNormal(),
                                            it->GetVertex(it->GetPoligon(j).GetVertexIndex(w - 1)).GetPosition() - it->GetVertex(it->GetPoligon(j).GetVertexIndex(w)).GetPosition(),
                                            direction_vector_,
                                            it->GetVertex(it->GetPoligon(j).GetVertexIndex(w)).GetPosition()));
                                    }

                                }

                            }
                        }
                    }
                }
            }
        }
        shadows_.push_back(shadow);
    }
}
