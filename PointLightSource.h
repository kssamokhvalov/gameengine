#pragma once
#include "LightSource.h"
class PointLightSource :
    public LightSource
{
public:
    PointLightSource();
    explicit PointLightSource(const Color& light_color, const Vector3& position);

    Color Shading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const override;
    void CreateShadowVolumes(std::vector<Shape>* list_objects) override;
private:
    bool IsPossitiveSide(const Vector3& normal, const Vector3& point);
    Vector3 position_;
    Color color_of_light_;
    std::vector<std::vector<Edge>> shadows_;
};

