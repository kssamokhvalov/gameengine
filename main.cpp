#include <glut.h>
#include <vector>
#include<iostream>

#include "Settings.h"
#include "Camera.h"
#include "Screen.h"
#include "Matrix4x4.h"
#include "Math/Vector3.h"
#include "Math/Quaternion.h"
#include "World.h"
#include "Shader.h"

using namespace std;

Camera playerCamera;
World world;
Shader shader(world.GetListLightSources(), Color(0.5f, 0.5f, 0.5f));;

float old_x = -1.0f;
float old_y = -1.0f;
bool ignore = false;

int Screen::render_width_screen_ = 0;
int Screen::render_hight_screen_ = 0;

int Screen::width_screen_ = 0;
int Screen::hight_screen_ = 0;

float Screen::alpha_coeff_screen_ = 0;
int Screen::offset_width_screen_ = 0;

std::vector<std::vector<Pixel>>* Screen::screen_buffer_;



void idleRender(void)
{
    world.Update();
    shader.DrawObjects(world.GetListObject(), playerCamera.GetTransformMatrixCameraToWorld());
    Screen::Update();
}

void KeyboardIvent(unsigned char key, int x, int y)
{
    std::cout << "Button: " << key << " X: " << x << " Y: " << y << "\n";
    
    const float speed_move = 0.1;

    switch (key)
    {
    case Keyboard::A:
        playerCamera.MoveRightOrLeft(-speed_move);
        break;
    case Keyboard::D:
        playerCamera.MoveRightOrLeft(speed_move);
        break;
    case Keyboard::S:
        playerCamera.MoveFowardOrBackward(-speed_move);
        break;
    case Keyboard::W:
        playerCamera.MoveFowardOrBackward(speed_move);
        break;
    case Keyboard::I:
        playerCamera.IncreasePitch(speed_move);
        break;
    case Keyboard::J:
        playerCamera.IncreaseYaw(speed_move);
        break;
    case Keyboard::K:
        playerCamera.IncreasePitch(-speed_move);
        break;
    case Keyboard::L:
        playerCamera.IncreaseYaw(-speed_move);
        break;
    default:
        break;
    }
}

void MouseIvent(int button, int state, int x, int y)
{
    std::cout << "Button: " << button << " State: " << state << " X: " << x << " Y: " << y << "\n";
}

void PassiveMotion(int x, int y) {
    std::cout << "Mouse moved: " << " X: " << x << " Y: " << y << "\n";
}

void(&Screen::update)(void) = idleRender;


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    Screen::SetScreenBuffer(shader.GetScreenBuffer());
    Screen::InitScreen();
    glutMouseFunc(MouseIvent);
    glutPassiveMotionFunc(PassiveMotion);
    glutKeyboardFunc(KeyboardIvent);
    glutMainLoop();
    return 0;
}