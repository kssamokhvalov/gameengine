#pragma once
#include <vector>

#include "Color.h"
#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Poligon.h"
#include "Shape.h"

struct Edge
{
	Vector4 top_plate_;
	Vector4 edge_plate_;
};

class LightSource
{
public:
	virtual Color Shading(const PropertyOfPoligon& property_poligon, const Vector3& normal, const Vector3& Q) const = 0;
	virtual void CreateShadowVolumes(std::vector<Shape>* list_objects) = 0;
	Edge CreateEdge(const Vector3& normal_poligon, const Vector3& direct_edge, const Vector3& direct_light, const Vector3& any_vertex_position);
	bool LyingInsideShadowVoluem(const std::vector<Edge>& voluem, const Vector3& point) const;
};

