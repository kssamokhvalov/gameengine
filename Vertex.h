#pragma once
#include "Math/Vector3.h"
class Vertex
{
public:
	Vertex(const float x, const float y, const float z);
	Vertex(const Vector3& position);

	float GetX() const;
	float GetY() const;
	float GetZ() const;

	const Vector3& GetPosition() const;
private:
	Vector3 position_;
};

