#include "Cube.h"

Cube::Cube()
{
	//Hard Code
	vertices_.push_back(Vertex(-length_a / 2, -length_a / 2, -length_a / 2));
	vertices_.push_back(Vertex(length_a / 2, -length_a / 2, -length_a / 2));
	vertices_.push_back(Vertex(length_a / 2, length_a / 2, -length_a / 2));
	vertices_.push_back(Vertex(-length_a / 2, length_a / 2, -length_a / 2));
	vertices_.push_back(Vertex(-length_a / 2, -length_a / 2, length_a / 2));
	vertices_.push_back(Vertex(length_a / 2, -length_a / 2, length_a / 2));
	vertices_.push_back(Vertex(length_a / 2, length_a / 2, length_a / 2));
	vertices_.push_back(Vertex(-length_a / 2, length_a / 2, length_a / 2));

	poligons_.push_back(Poligon(0, 2, 1));
	poligons_.push_back(Poligon(0, 3, 2));

	poligons_.push_back(Poligon(0, 1, 5));
	poligons_.push_back(Poligon(0, 5, 4));

	poligons_.push_back(Poligon(1, 2, 6));
	poligons_.push_back(Poligon(1, 6, 5));

	poligons_.push_back(Poligon(2, 3, 7));
	poligons_.push_back(Poligon(2, 7, 6));

	poligons_.push_back(Poligon(3, 0, 4));
	poligons_.push_back(Poligon(3, 4, 7));

	poligons_.push_back(Poligon(4, 5, 6));
	poligons_.push_back(Poligon(4, 6, 7));

	for (auto it = std::begin(poligons_); it != std::end(poligons_); ++it) {
		//it->SetColor(0.5, 0.5, 0.5);
		it->SetNormal(Vector3::Cross((vertices_.at(it->GetSecondVertexIndex()).GetPosition() - vertices_.at(it->GetFirstVertexIndex()).GetPosition()),
			(vertices_.at(it->GetThirdVertexIndex()).GetPosition() - vertices_.at(it->GetFirstVertexIndex()).GetPosition())));
	}
	position_ = Vector3(0, 0, -2);
}

const Vertex& Cube::GetVertex(const int& index) const
{
	return vertices_.at(index);
}

const Poligon& Cube::GetPoligon(const int& index) const
{
	return poligons_.at(index);
}

const Vector3& Cube::GetPosition() const
{
	return position_;
}

void Cube::SetPosition(const Vector3& position)
{
	position_ = position;
}

const int& Cube::GetNumPoligons() const
{
	return poligons_.size();
}
