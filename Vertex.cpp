#include "Vertex.h"

Vertex::Vertex(const float x, const float y, const float z)
{
	position_ = Vector3(x, y, z);
}

Vertex::Vertex(const Vector3& position)
{
	position_ = position;
}

float Vertex::GetX() const
{
	return position_.GetX();
}

float Vertex::GetY() const
{
	return position_.GetY();;
}

float Vertex::GetZ() const
{
	return position_.GetZ();
}

const Vector3& Vertex::GetPosition() const
{
	return position_;
}
