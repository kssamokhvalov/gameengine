#pragma once
#include "Math/Vector3.h"
#include "Settings.h"
#include <glut.h>

#include "Matrix4x4.h"

class Camera
{
public:
    Camera();

    void IncreaseYaw(const float& delta_yaw);
    void IncreasePitch(const float& delta_pitch);
    void MoveFowardOrBackward(const float& delta);
    void MoveRightOrLeft(const float& delta);

    Matrix4x4 GetTransformMatrixCameraToWorld() const;

private:
    Vector3 viewer_vector_; // world space
    Vector3 position_; // world space

    float pitch_;
    float roll_;
    float yaw_;
};

