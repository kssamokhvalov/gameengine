#pragma once
#include <glut.h>
#include <vector>
#include "Settings.h"
#include "Shader.h"



class Screen
{
public:
	static void InitScreen();

	static void Display();
	static void Update();
	static void Resize(int width, int height);
	static void SetScreenBuffer(std::vector<std::vector<Pixel>>* screen_buffer);

	static int render_width_screen_;
	static int render_hight_screen_;
	static int width_screen_;
	static int hight_screen_;
	static float alpha_coeff_screen_;
	static int offset_width_screen_;

	static std::vector<std::vector<Pixel>> *screen_buffer_;
	static void(&update)(void);
private:
	static void SetPixel(const GLint& x, const GLint& y);
	static void ClearPixel(Pixel& pixel);
};

