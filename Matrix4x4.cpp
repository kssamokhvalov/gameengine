#include "Matrix4x4.h"

Matrix4x4 Matrix4x4::CreateRotationMatrix(const Vector3& axis_A, const float& angle_radian)
{
    //maybe better use SIMD
    Matrix4x4 ans_matrix;

    ans_matrix.matrix_[0][0] = cosf(angle_radian) + (1 - cosf(angle_radian)) * axis_A.GetX() * axis_A.GetX();
    ans_matrix.matrix_[0][1] = (1 - cosf(angle_radian)) * axis_A.GetX() * axis_A.GetY() - sinf(angle_radian) * axis_A.GetZ();
    ans_matrix.matrix_[0][2] = (1 - cosf(angle_radian)) * axis_A.GetX() * axis_A.GetZ() + sinf(angle_radian) * axis_A.GetY();
    ans_matrix.matrix_[0][3] = 0;

    ans_matrix.matrix_[1][0] = (1 - cosf(angle_radian)) * axis_A.GetX() * axis_A.GetY() + sinf(angle_radian) * axis_A.GetZ();
    ans_matrix.matrix_[1][1] = cosf(angle_radian) + (1 - cosf(angle_radian)) * axis_A.GetY() * axis_A.GetY();
    ans_matrix.matrix_[1][2] = (1 - cosf(angle_radian)) * axis_A.GetY() * axis_A.GetZ() - sinf(angle_radian) * axis_A.GetX();
    ans_matrix.matrix_[1][3] = 0;

    ans_matrix.matrix_[2][0] = (1 - cosf(angle_radian)) * axis_A.GetX() * axis_A.GetZ() - sinf(angle_radian) * axis_A.GetY();
    ans_matrix.matrix_[2][1] = (1 - cosf(angle_radian)) * axis_A.GetY() * axis_A.GetZ() + sinf(angle_radian) * axis_A.GetX();
    ans_matrix.matrix_[2][2] = cosf(angle_radian) + (1 - cosf(angle_radian)) * axis_A.GetZ() * axis_A.GetZ();
    ans_matrix.matrix_[2][3] = 0;

    ans_matrix.matrix_[3][0] = 0;
    ans_matrix.matrix_[3][1] = 0;
    ans_matrix.matrix_[3][2] = 0;
    ans_matrix.matrix_[3][3] = 1;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::CreateRotationMatrix(const Quaternion& quaternion)
{
    //maybe better use SIMD
    Matrix4x4 ans_matrix;

    ans_matrix.matrix_[0][0] = 1 - 2 * quaternion.GetJ() * quaternion.GetJ() - 2 * quaternion.GetK() * quaternion.GetK();
    ans_matrix.matrix_[0][1] = 2 * quaternion.GetI() * quaternion.GetJ() - 2 * quaternion.GetW() * quaternion.GetK();
    ans_matrix.matrix_[0][2] = 2 * quaternion.GetI() * quaternion.GetK() + 2 * quaternion.GetW() * quaternion.GetJ();
    ans_matrix.matrix_[0][3] = 0;

    ans_matrix.matrix_[1][0] = 2 * quaternion.GetI() * quaternion.GetJ() + 2 * quaternion.GetW() * quaternion.GetK();
    ans_matrix.matrix_[1][1] = 1 - 2 * quaternion.GetI() * quaternion.GetI() - 2 * quaternion.GetK() * quaternion.GetK();
    ans_matrix.matrix_[1][2] = 2 * quaternion.GetJ() * quaternion.GetK() - 2 * quaternion.GetW() * quaternion.GetI();
    ans_matrix.matrix_[1][3] = 0;

    ans_matrix.matrix_[2][0] = 2 * quaternion.GetI() * quaternion.GetK() - 2 * quaternion.GetW() * quaternion.GetJ();
    ans_matrix.matrix_[2][1] = 2 * quaternion.GetJ() * quaternion.GetK() + 2 * quaternion.GetW() * quaternion.GetI();
    ans_matrix.matrix_[2][2] = 1 - 2 * quaternion.GetI() * quaternion.GetI() - 2 * quaternion.GetJ() * quaternion.GetJ();
    ans_matrix.matrix_[2][3] = 0;

    ans_matrix.matrix_[3][0] = 0;
    ans_matrix.matrix_[3][1] = 0;
    ans_matrix.matrix_[3][2] = 0;
    ans_matrix.matrix_[3][3] = 1;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::CreateTransformationMatrix(const Vector3& axis_A, const float& angle_radian, const Vector3& vector_T)
{
    return CreateTransformationMatrix(CreateRotationMatrix(axis_A, angle_radian), vector_T);
}

Matrix4x4 Matrix4x4::CreateTransformationMatrix(const Quaternion& quaternion, const Vector3& vector_T)
{
    return CreateTransformationMatrix(CreateRotationMatrix(quaternion), vector_T);
}

Matrix4x4 Matrix4x4::CreateTransformationMatrix(const Matrix4x4& rotation_matrix, const Vector3& vector_T)
{
    Matrix4x4 ans_matrix = rotation_matrix;

    ans_matrix.matrix_[0][3] = vector_T.GetX();
    ans_matrix.matrix_[1][3] = vector_T.GetY();
    ans_matrix.matrix_[2][3] = vector_T.GetZ();

    return ans_matrix;
}

Matrix4x4 Matrix4x4::CreateZeroMatrix()
{
    Matrix4x4 ans_matrix;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            ans_matrix.matrix_[i][j] = 0;
        }
    }

    return ans_matrix;
}

Matrix4x4 Matrix4x4::CreateOnesMatrix()
{
    Matrix4x4 ans_matrix;

    ans_matrix.matrix_[0][0] = 23;
    ans_matrix.matrix_[0][1] = 7;
    ans_matrix.matrix_[0][2] = 2;
    ans_matrix.matrix_[0][3] = 1;

    ans_matrix.matrix_[1][0] = 4;
    ans_matrix.matrix_[1][1] = 2;
    ans_matrix.matrix_[1][2] = 7;
    ans_matrix.matrix_[1][3] = 2;

    ans_matrix.matrix_[2][0] = 1;
    ans_matrix.matrix_[2][1] = 4;
    ans_matrix.matrix_[2][2] = 23;
    ans_matrix.matrix_[2][3] = 7;

    ans_matrix.matrix_[3][0] = 8;
    ans_matrix.matrix_[3][1] = 2;
    ans_matrix.matrix_[3][2] = 4;
    ans_matrix.matrix_[3][3] = 2;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::CreateFrustumProjectionMatrix(const float& angle_FOV, const float& aspect, const float& near_plate, const float& far_plate)
{
    Matrix4x4 ans_matrix = CreateZeroMatrix();

    ans_matrix.matrix_[0][0] = 1 / tanf(angle_FOV / 2);
    ans_matrix.matrix_[1][1] = 1 / (tanf(angle_FOV / 2) * aspect);
    ans_matrix.matrix_[2][2] = -(far_plate + ans_matrix.matrix_[0][0])/ (far_plate - ans_matrix.matrix_[0][0]);
    ans_matrix.matrix_[2][3] = -2 * far_plate * ans_matrix.matrix_[0][0] / (far_plate - ans_matrix.matrix_[0][0]);;
    ans_matrix.matrix_[3][2] = -1;

    return ans_matrix;
}

Vector4 Matrix4x4::operator*(const Vector4& vector) const
{
    //maybe better use SIMD
    float x = vector.GetX() * matrix_[0][0] + vector.GetY() * matrix_[0][1] + vector.GetZ() * matrix_[0][2] + vector.GetW() * matrix_[0][3];
    float y = vector.GetX() * matrix_[1][0] + vector.GetY() * matrix_[1][1] + vector.GetZ() * matrix_[1][2] + vector.GetW() * matrix_[1][3];
    float z = vector.GetX() * matrix_[2][0] + vector.GetY() * matrix_[2][1] + vector.GetZ() * matrix_[2][2] + vector.GetW() * matrix_[2][3];
    float w = vector.GetX() * matrix_[3][0] + vector.GetY() * matrix_[3][1] + vector.GetZ() * matrix_[3][2] + vector.GetW() * matrix_[3][3];

    return Vector4(x, y, z, w);
}

Matrix4x4 Matrix4x4::operator*(const Matrix4x4& input_matrix) const
{
    //maybe better use SIMD
    Matrix4x4 ans_matrix = CreateZeroMatrix();

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                ans_matrix.matrix_[i][j] += matrix_[k][j] * input_matrix.matrix_[i][k];
            }
        }
    }

    return ans_matrix;
}

void Matrix4x4::operator*=(const Matrix4x4& input_matrix)
{
    Matrix4x4 ans_matrix = CreateZeroMatrix();

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                ans_matrix.matrix_[i][j] += matrix_[k][j] * input_matrix.matrix_[i][k];
            }
        }
    }
    *this = ans_matrix;
}

void Matrix4x4::operator/=(const float& num)
{
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matrix_[i][j] /= num;
        }
    }
}

void Matrix4x4::operator=(const Matrix4x4& input_matrix)
{
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matrix_[i][j] = input_matrix.matrix_[i][j];
        }
    }
}

Vector3 Matrix4x4::ProductOnVector3(const Vector3& vector, const float& vector_w) const
{
    float x = vector.GetX() * matrix_[0][0] + vector.GetY() * matrix_[0][1] + vector.GetZ() * matrix_[0][2] + vector_w * matrix_[0][3];
    float y = vector.GetX() * matrix_[1][0] + vector.GetY() * matrix_[1][1] + vector.GetZ() * matrix_[1][2] + vector_w * matrix_[1][3];
    float z = vector.GetX() * matrix_[2][0] + vector.GetY() * matrix_[2][1] + vector.GetZ() * matrix_[2][2] + vector_w * matrix_[2][3];
    float w = vector.GetX() * matrix_[3][0] + vector.GetY() * matrix_[3][1] + vector.GetZ() * matrix_[3][2] + vector_w * matrix_[3][3];

    return Vector3(x / w, y / w, z / w);
}

Vector3 Matrix4x4::ProductOnVector3ForVectors(const Vector3& vector) const
{
    float x = vector.GetX() * matrix_[0][0] + vector.GetY() * matrix_[0][1] + vector.GetZ() * matrix_[0][2];
    float y = vector.GetX() * matrix_[1][0] + vector.GetY() * matrix_[1][1] + vector.GetZ() * matrix_[1][2];
    float z = vector.GetX() * matrix_[2][0] + vector.GetY() * matrix_[2][1] + vector.GetZ() * matrix_[2][2];

    return Vector3(x, y, z);
}

float Matrix4x4::GetDeterminant() const
{
    return matrix_[0][0] * matrix_[1][1] * matrix_[2][2] * matrix_[3][3] + matrix_[0][0] * matrix_[2][1] * matrix_[3][2] * matrix_[1][3]
        + matrix_[0][0] * matrix_[3][1] * matrix_[1][2] * matrix_[2][3] - matrix_[0][0] * matrix_[3][1] * matrix_[2][2] * matrix_[1][3]
        - matrix_[0][0] * matrix_[2][1] * matrix_[1][2] * matrix_[3][3] - matrix_[0][0] * matrix_[1][1] * matrix_[3][2] * matrix_[2][3]
        - matrix_[1][0] * matrix_[0][1] * matrix_[2][2] * matrix_[3][3] - matrix_[2][0] * matrix_[0][1] * matrix_[3][2] * matrix_[1][3]
        - matrix_[3][0] * matrix_[0][1] * matrix_[1][2] * matrix_[2][3] + matrix_[3][0] * matrix_[0][1] * matrix_[2][2] * matrix_[1][3]
        + matrix_[2][0] * matrix_[0][1] * matrix_[1][2] * matrix_[3][3] + matrix_[1][0] * matrix_[0][1] * matrix_[3][2] * matrix_[2][3]
        + matrix_[1][0] * matrix_[2][1] * matrix_[0][2] * matrix_[3][3] + matrix_[2][0] * matrix_[3][1] * matrix_[0][2] * matrix_[1][3]
        + matrix_[3][0] * matrix_[1][1] * matrix_[0][2] * matrix_[2][3] - matrix_[3][0] * matrix_[2][1] * matrix_[0][2] * matrix_[1][3]
        - matrix_[2][0] * matrix_[1][1] * matrix_[0][2] * matrix_[3][3] - matrix_[1][0] * matrix_[3][1] * matrix_[0][2] * matrix_[2][3]
        - matrix_[1][0] * matrix_[2][1] * matrix_[3][2] * matrix_[0][3] - matrix_[2][0] * matrix_[3][1] * matrix_[1][2] * matrix_[0][3]
        - matrix_[3][0] * matrix_[1][1] * matrix_[2][2] * matrix_[0][3] + matrix_[3][0] * matrix_[2][1] * matrix_[1][2] * matrix_[0][3]
        + matrix_[2][0] * matrix_[1][1] * matrix_[3][2] * matrix_[0][3] + matrix_[1][0] * matrix_[3][1] * matrix_[2][2] * matrix_[0][3];
}

void Matrix4x4::InverseMatrix()
{
    Matrix4x4 ans_matrix = CreateZeroMatrix();

    ans_matrix.matrix_[0][0] = matrix_[1][1] * matrix_[2][2] * matrix_[3][3] + matrix_[2][1] * matrix_[3][2] * matrix_[1][3]
        + matrix_[3][1] * matrix_[1][2] * matrix_[2][3] - matrix_[3][1] * matrix_[2][2] * matrix_[1][3]
        - matrix_[2][1] * matrix_[1][2] * matrix_[3][3] - matrix_[1][1] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[0][1] = -matrix_[0][1] * matrix_[2][2] * matrix_[3][3] - matrix_[2][1] * matrix_[3][2] * matrix_[0][3]
        - matrix_[3][1] * matrix_[0][2] * matrix_[2][3] + matrix_[3][1] * matrix_[2][2] * matrix_[0][3]
        + matrix_[2][1] * matrix_[0][2] * matrix_[3][3] + matrix_[0][1] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[0][2] = matrix_[0][1] * matrix_[1][2] * matrix_[3][3] + matrix_[1][1] * matrix_[3][2] * matrix_[0][3]
        + matrix_[3][1] * matrix_[0][2] * matrix_[1][3] - matrix_[3][1] * matrix_[1][2] * matrix_[0][3]
        - matrix_[1][1] * matrix_[0][2] * matrix_[3][3] - matrix_[0][1] * matrix_[3][2] * matrix_[1][3];

    ans_matrix.matrix_[0][3] = -matrix_[0][1] * matrix_[1][2] * matrix_[2][3] - matrix_[1][1] * matrix_[2][2] * matrix_[0][3]
        - matrix_[2][1] * matrix_[0][2] * matrix_[1][3] + matrix_[2][1] * matrix_[1][2] * matrix_[0][3]
        + matrix_[1][1] * matrix_[0][2] * matrix_[2][3] + matrix_[0][1] * matrix_[2][2] * matrix_[1][3];



    ans_matrix.matrix_[1][0] = -matrix_[1][0] * matrix_[2][2] * matrix_[3][3] - matrix_[2][0] * matrix_[3][2] * matrix_[1][3]
        - matrix_[3][0] * matrix_[1][2] * matrix_[2][3] + matrix_[3][0] * matrix_[2][2] * matrix_[1][3]
        + matrix_[2][0] * matrix_[1][2] * matrix_[3][3] + matrix_[1][0] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[1][1] = matrix_[0][0] * matrix_[2][2] * matrix_[3][3] + matrix_[2][0] * matrix_[3][2] * matrix_[0][3]
        + matrix_[3][0] * matrix_[0][2] * matrix_[2][3] - matrix_[3][0] * matrix_[2][2] * matrix_[0][3]
        - matrix_[2][0] * matrix_[0][2] * matrix_[3][3] - matrix_[0][0] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[1][2] = -matrix_[0][0] * matrix_[1][2] * matrix_[3][3] - matrix_[1][0] * matrix_[3][2] * matrix_[0][3]
        - matrix_[3][0] * matrix_[0][2] * matrix_[1][3] + matrix_[3][0] * matrix_[1][2] * matrix_[0][3]
        + matrix_[1][0] * matrix_[0][2] * matrix_[3][3] + matrix_[0][0] * matrix_[3][2] * matrix_[1][3];

    ans_matrix.matrix_[1][3] = matrix_[0][0] * matrix_[1][2] * matrix_[2][3] + matrix_[1][0] * matrix_[2][2] * matrix_[0][3]
        + matrix_[2][0] * matrix_[0][2] * matrix_[1][3] - matrix_[2][0] * matrix_[1][2] * matrix_[0][3]
        - matrix_[1][0] * matrix_[0][2] * matrix_[2][3] - matrix_[0][0] * matrix_[2][2] * matrix_[1][3];



    ans_matrix.matrix_[2][0] = matrix_[1][0] * matrix_[2][1] * matrix_[3][3] + matrix_[2][0] * matrix_[3][1] * matrix_[1][3]
        + matrix_[3][0] * matrix_[1][1] * matrix_[2][3] - matrix_[3][0] * matrix_[2][1] * matrix_[1][3]
        - matrix_[2][0] * matrix_[1][1] * matrix_[3][3] - matrix_[1][0] * matrix_[3][1] * matrix_[2][3];

    ans_matrix.matrix_[2][1] = -matrix_[0][0] * matrix_[2][1] * matrix_[3][3] - matrix_[2][0] * matrix_[3][1] * matrix_[0][3]
        - matrix_[3][0] * matrix_[0][1] * matrix_[2][3] + matrix_[3][0] * matrix_[2][1] * matrix_[0][3]
        + matrix_[2][0] * matrix_[0][1] * matrix_[3][3] + matrix_[0][0] * matrix_[3][1] * matrix_[2][3];

    ans_matrix.matrix_[2][2] = matrix_[0][0] * matrix_[1][1] * matrix_[3][3] + matrix_[1][0] * matrix_[3][1] * matrix_[0][3]
        + matrix_[3][0] * matrix_[0][1] * matrix_[1][3] - matrix_[3][0] * matrix_[1][1] * matrix_[0][3]
        - matrix_[1][0] * matrix_[0][1] * matrix_[3][3] - matrix_[0][0] * matrix_[3][1] * matrix_[1][3];

    ans_matrix.matrix_[2][3] = -matrix_[0][0] * matrix_[1][1] * matrix_[2][3] - matrix_[1][0] * matrix_[2][1] * matrix_[0][3]
        - matrix_[2][0] * matrix_[0][1] * matrix_[1][3] + matrix_[2][0] * matrix_[1][1] * matrix_[0][3]
        + matrix_[1][0] * matrix_[0][1] * matrix_[2][3] + matrix_[0][0] * matrix_[2][1] * matrix_[1][3];



    ans_matrix.matrix_[3][0] = -matrix_[1][0] * matrix_[2][1] * matrix_[3][2] - matrix_[2][0] * matrix_[3][1] * matrix_[1][2]
        - matrix_[3][0] * matrix_[1][1] * matrix_[2][2] + matrix_[3][0] * matrix_[2][1] * matrix_[1][2]
        + matrix_[2][0] * matrix_[1][1] * matrix_[3][2] + matrix_[1][0] * matrix_[3][1] * matrix_[2][2];

    ans_matrix.matrix_[3][1] = matrix_[0][0] * matrix_[2][1] * matrix_[3][2] + matrix_[2][0] * matrix_[3][1] * matrix_[0][2]
        + matrix_[3][0] * matrix_[0][1] * matrix_[2][2] - matrix_[3][0] * matrix_[2][1] * matrix_[0][2]
        - matrix_[2][0] * matrix_[0][1] * matrix_[3][2] - matrix_[0][0] * matrix_[3][1] * matrix_[2][2];

    ans_matrix.matrix_[3][2] = -matrix_[0][0] * matrix_[1][1] * matrix_[3][2] - matrix_[1][0] * matrix_[3][1] * matrix_[0][2]
        - matrix_[3][0] * matrix_[0][1] * matrix_[1][2] + matrix_[3][0] * matrix_[1][1] * matrix_[0][2]
        + matrix_[1][0] * matrix_[0][1] * matrix_[3][2] + matrix_[0][0] * matrix_[3][1] * matrix_[1][2];

    ans_matrix.matrix_[3][3] = matrix_[0][0] * matrix_[1][1] * matrix_[2][2] + matrix_[1][0] * matrix_[2][1] * matrix_[0][2]
        + matrix_[2][0] * matrix_[0][1] * matrix_[1][2] - matrix_[2][0] * matrix_[1][1] * matrix_[0][2]
        - matrix_[1][0] * matrix_[0][1] * matrix_[2][2] - matrix_[0][0] * matrix_[2][1] * matrix_[1][2];

    ans_matrix /= GetDeterminant();

    *this = ans_matrix;
}

Matrix4x4 Matrix4x4::GetInversedMatrix() const
{
    Matrix4x4 ans_matrix = CreateZeroMatrix();

    ans_matrix.matrix_[0][0] = matrix_[1][1] * matrix_[2][2] * matrix_[3][3] + matrix_[2][1] * matrix_[3][2] * matrix_[1][3]
        + matrix_[3][1] * matrix_[1][2] * matrix_[2][3] - matrix_[3][1] * matrix_[2][2] * matrix_[1][3]
        - matrix_[2][1] * matrix_[1][2] * matrix_[3][3] - matrix_[1][1] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[0][1] = - matrix_[0][1] * matrix_[2][2] * matrix_[3][3] - matrix_[2][1] * matrix_[3][2] * matrix_[0][3]
        - matrix_[3][1] * matrix_[0][2] * matrix_[2][3] + matrix_[3][1] * matrix_[2][2] * matrix_[0][3]
        + matrix_[2][1] * matrix_[0][2] * matrix_[3][3] + matrix_[0][1] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[0][2] = matrix_[0][1] * matrix_[1][2] * matrix_[3][3] + matrix_[1][1] * matrix_[3][2] * matrix_[0][3]
        + matrix_[3][1] * matrix_[0][2] * matrix_[1][3] - matrix_[3][1] * matrix_[1][2] * matrix_[0][3]
        - matrix_[1][1] * matrix_[0][2] * matrix_[3][3] - matrix_[0][1] * matrix_[3][2] * matrix_[1][3];

    ans_matrix.matrix_[0][3] = - matrix_[0][1] * matrix_[1][2] * matrix_[2][3] - matrix_[1][1] * matrix_[2][2] * matrix_[0][3]
        - matrix_[2][1] * matrix_[0][2] * matrix_[1][3] + matrix_[2][1] * matrix_[1][2] * matrix_[0][3]
        + matrix_[1][1] * matrix_[0][2] * matrix_[2][3] + matrix_[0][1] * matrix_[2][2] * matrix_[1][3];



    ans_matrix.matrix_[1][0] = - matrix_[1][0] * matrix_[2][2] * matrix_[3][3] - matrix_[2][0] * matrix_[3][2] * matrix_[1][3]
        - matrix_[3][0] * matrix_[1][2] * matrix_[2][3] + matrix_[3][0] * matrix_[2][2] * matrix_[1][3]
        + matrix_[2][0] * matrix_[1][2] * matrix_[3][3] + matrix_[1][0] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[1][1] = matrix_[0][0] * matrix_[2][2] * matrix_[3][3] + matrix_[2][0] * matrix_[3][2] * matrix_[0][3]
        + matrix_[3][0] * matrix_[0][2] * matrix_[2][3] - matrix_[3][0] * matrix_[2][2] * matrix_[0][3]
        - matrix_[2][0] * matrix_[0][2] * matrix_[3][3] - matrix_[0][0] * matrix_[3][2] * matrix_[2][3];

    ans_matrix.matrix_[1][2] = - matrix_[0][0] * matrix_[1][2] * matrix_[3][3] - matrix_[1][0] * matrix_[3][2] * matrix_[0][3]
        - matrix_[3][0] * matrix_[0][2] * matrix_[1][3] + matrix_[3][0] * matrix_[1][2] * matrix_[0][3]
        + matrix_[1][0] * matrix_[0][2] * matrix_[3][3] + matrix_[0][0] * matrix_[3][2] * matrix_[1][3];

    ans_matrix.matrix_[1][3] = matrix_[0][0] * matrix_[1][2] * matrix_[2][3] + matrix_[1][0] * matrix_[2][2] * matrix_[0][3]
        + matrix_[2][0] * matrix_[0][2] * matrix_[1][3] - matrix_[2][0] * matrix_[1][2] * matrix_[0][3]
        - matrix_[1][0] * matrix_[0][2] * matrix_[2][3] - matrix_[0][0] * matrix_[2][2] * matrix_[1][3];



    ans_matrix.matrix_[2][0] = matrix_[1][0] * matrix_[2][1] * matrix_[3][3] + matrix_[2][0] * matrix_[3][1] * matrix_[1][3]
        + matrix_[3][0] * matrix_[1][1] * matrix_[2][3] - matrix_[3][0] * matrix_[2][1] * matrix_[1][3]
        - matrix_[2][0] * matrix_[1][1] * matrix_[3][3] - matrix_[1][0] * matrix_[3][1] * matrix_[2][3];

    ans_matrix.matrix_[2][1] = -matrix_[0][0] * matrix_[2][1] * matrix_[3][3] - matrix_[2][0] * matrix_[3][1] * matrix_[0][3]
        - matrix_[3][0] * matrix_[0][1] * matrix_[2][3] + matrix_[3][0] * matrix_[2][1] * matrix_[0][3]
        + matrix_[2][0] * matrix_[0][1] * matrix_[3][3] + matrix_[0][0] * matrix_[3][1] * matrix_[2][3];

    ans_matrix.matrix_[2][2] = matrix_[0][0] * matrix_[1][1] * matrix_[3][3] + matrix_[1][0] * matrix_[3][1] * matrix_[0][3]
        + matrix_[3][0] * matrix_[0][1] * matrix_[1][3] - matrix_[3][0] * matrix_[1][1] * matrix_[0][3]
        - matrix_[1][0] * matrix_[0][1] * matrix_[3][3] - matrix_[0][0] * matrix_[3][1] * matrix_[1][3];

    ans_matrix.matrix_[2][3] = -matrix_[0][0] * matrix_[1][1] * matrix_[2][3] - matrix_[1][0] * matrix_[2][1] * matrix_[0][3]
        - matrix_[2][0] * matrix_[0][1] * matrix_[1][3] + matrix_[2][0] * matrix_[1][1] * matrix_[0][3]
        + matrix_[1][0] * matrix_[0][1] * matrix_[2][3] + matrix_[0][0] * matrix_[2][1] * matrix_[1][3];



    ans_matrix.matrix_[3][0] = - matrix_[1][0] * matrix_[2][1] * matrix_[3][2] - matrix_[2][0] * matrix_[3][1] * matrix_[1][2]
        - matrix_[3][0] * matrix_[1][1] * matrix_[2][2] + matrix_[3][0] * matrix_[2][1] * matrix_[1][2]
        + matrix_[2][0] * matrix_[1][1] * matrix_[3][2] + matrix_[1][0] * matrix_[3][1] * matrix_[2][2];

    ans_matrix.matrix_[3][1] = matrix_[0][0] * matrix_[2][1] * matrix_[3][2] + matrix_[2][0] * matrix_[3][1] * matrix_[0][2]
        + matrix_[3][0] * matrix_[0][1] * matrix_[2][2] - matrix_[3][0] * matrix_[2][1] * matrix_[0][2]
        - matrix_[2][0] * matrix_[0][1] * matrix_[3][2] - matrix_[0][0] * matrix_[3][1] * matrix_[2][2];

    ans_matrix.matrix_[3][2] = - matrix_[0][0] * matrix_[1][1] * matrix_[3][2] - matrix_[1][0] * matrix_[3][1] * matrix_[0][2]
        - matrix_[3][0] * matrix_[0][1] * matrix_[1][2] + matrix_[3][0] * matrix_[1][1] * matrix_[0][2]
        + matrix_[1][0] * matrix_[0][1] * matrix_[3][2] + matrix_[0][0] * matrix_[3][1] * matrix_[1][2];

    ans_matrix.matrix_[3][3] = matrix_[0][0] * matrix_[1][1] * matrix_[2][2] + matrix_[1][0] * matrix_[2][1] * matrix_[0][2]
        + matrix_[2][0] * matrix_[0][1] * matrix_[1][2] - matrix_[2][0] * matrix_[1][1] * matrix_[0][2]
        - matrix_[1][0] * matrix_[0][1] * matrix_[2][2] - matrix_[0][0] * matrix_[2][1] * matrix_[1][2];

    ans_matrix /= GetDeterminant();

    return ans_matrix;
}

void Matrix4x4::ChangeHandedness()
{
    Matrix4x4 ans_matrix = CreateZeroMatrix();
    ans_matrix.matrix_[0][0] = 1;
    ans_matrix.matrix_[1][1] = 1;
    ans_matrix.matrix_[2][2] = -1;
    ans_matrix.matrix_[3][3] = 1;

    *this *= ans_matrix;
}

Matrix4x4 Matrix4x4::GetMatrixInverses(const Matrix4x4& matrix)
{
    //maybe better use SIMD
    return matrix.GetInversedMatrix();
}
